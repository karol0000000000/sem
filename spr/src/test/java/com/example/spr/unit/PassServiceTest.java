package com.example.spr.unit;

import com.example.spr.service.PassService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class PassServiceTest {

    @Autowired
    private PassService passService;
    private final String mySecretPass = "mySecretPass";
    private final String encrypted = "U2FsdGVkX19A8s1Y4g5vtQI44Tvu1qXKr6wRXY5wfD8=";

    @Test
    public void should_decode_pass(){

        String decoded = passService.decodePass(encrypted);
        Assert.assertEquals(decoded, mySecretPass);
    }

    @Test
    public void should_return_not_equal_pass(){

        String decoded = passService.decodePass(encrypted);
        Assert.assertNotEquals(encrypted, mySecretPass);
    }
}
