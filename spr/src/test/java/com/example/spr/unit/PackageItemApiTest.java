package com.example.spr.unit;

import com.example.spr.models.entities.License;
import com.example.spr.models.entities.PackageItem;
import com.example.spr.repository.PackageItemRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.ws.rs.core.MediaType;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class PackageItemApiTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PackageItemRepository packageItemRepository;

    @Test
    public void should_package_item_endpoint_return_package_item__array_unit() throws Exception {

        List<PackageItem> allPackageItems = Collections.singletonList(new PackageItem("TEST", 100, new License()));
        Mockito.when(packageItemRepository.findAll()).thenReturn(allPackageItems);

        mvc.perform(get("/open/package-item")
                    .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is("TEST")));
   }
}
