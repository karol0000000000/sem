package com.example.spr.unit;

import com.example.spr.models.entities.Person;
import com.example.spr.models.entities.PackageItem;
import com.example.spr.models.payload_models.PersonGenerate;
import com.example.spr.models.payload_models.PersonType;
import com.example.spr.repository.PackageItemPersonRepository;
import com.example.spr.repository.PersonRepository;
import com.example.spr.service.PassService;
import com.example.spr.service.KeycloakService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.ws.rs.core.MediaType;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class AuthenticationApiTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private KeycloakService keycloakService;

    @MockBean
    private PersonRepository personRepository;

    @MockBean
    private PassService passService;

    @MockBean
    private PackageItemPersonRepository packageItemPersonRepository;

    @Spy
    private List<PackageItem> items = new ArrayList<>();

    @Test
    public void should_return_passed_user_to_registration() throws Exception {

        PersonGenerate personGenerate = new PersonGenerate("username", "password", "name", "surname", "mail", "123456789", items,
                "Warsaw", "02-100", "Mickiewicza", 1234, 20, "12345678900");
        Person person = new Person(personGenerate.getUsername(), personGenerate.getName(), personGenerate.getSurname(), personGenerate.getEmail(), personGenerate.getPhone(), PersonType.PATIENT,
                personGenerate.getCity(), personGenerate.getPostalCode(), personGenerate.getStreet(), personGenerate.getHouseNumber(), personGenerate.getApartmentNumber(), personGenerate.getPesel());

        Mockito.when(keycloakService.registerUser(Mockito.any(PersonGenerate.class))).thenReturn(200);
        Mockito.when(personRepository.save(Mockito.any(Person.class))).thenReturn(person);
        Mockito.when(passService.decodePass(Mockito.any(String.class))).thenReturn("newPass");

        mvc.perform(post("/open/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(personGenerate)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(person.getName())));
    }
}