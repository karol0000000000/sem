package com.example.spr.integration;

import com.example.spr.models.entities.License;
import com.example.spr.models.entities.PackageItem;
import com.example.spr.models.payload_models.LicenseType;
import com.example.spr.repository.LicenseRepository;
import com.example.spr.repository.PackageItemRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class PackageItemApiTest {

    @Autowired
    private WebApplicationContext context;
    private MockMvc mvc;

    @Autowired
    private PackageItemRepository packageItemRepository;

    @Autowired
    private LicenseRepository licenseRepository;

    @Before
    public void setUp(){
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void should_package_item_endpoint_return_package_item_array_integration() throws Exception {

        License testLicense = licenseRepository.save(new License(LicenseType.VISIT, "test-license"));
        List<PackageItem> allItems = Collections.singletonList(new PackageItem("test-item", 50, testLicense));
        packageItemRepository.saveAll(allItems);

        mvc.perform(get("/open/package-item")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is("test-item")));
    }

    @After
    public void clean(){
        packageItemRepository.deleteAll();
        licenseRepository.deleteAll();
    }
}
