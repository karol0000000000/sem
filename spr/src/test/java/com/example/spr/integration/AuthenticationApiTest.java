package com.example.spr.integration;

import com.example.spr.models.entities.License;
import com.example.spr.models.entities.PackageItem;
import com.example.spr.models.payload_models.LicenseType;
import com.example.spr.models.payload_models.PersonGenerate;
import com.example.spr.repository.LicenseRepository;
import com.example.spr.repository.PackageItemRepository;
import com.example.spr.repository.PackageItemPersonRepository;
import com.example.spr.repository.PersonRepository;
import com.example.spr.service.KeycloakService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;

import static org.hamcrest.core.Is.is;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
@Slf4j
public class AuthenticationApiTest {

    private final String myPass = "mySecretPass";
    private final String encrypted = "U2FsdGVkX19A8s1Y4g5vtQI44Tvu1qXKr6wRXY5wfD8=";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext context;
    private MockMvc mvc;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PackageItemRepository packageItemRepository;

    @Autowired
    private KeycloakService keycloakService;

    @Autowired
    private LicenseRepository licenseRepository;

    @Autowired
    private PackageItemPersonRepository packageItemPersonRepository;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        License testLicense = licenseRepository.save(new License(LicenseType.TEST, "test-license"));
        packageItemRepository.save(new PackageItem("test-item", 50, testLicense));
    }

    @After
    public void clean() {
        int status = keycloakService.deleteUser("username");
        if (status >= 200 && status <= 204) {
            log.error("Error while deleting user");
        }

        packageItemPersonRepository.deleteAll();
        packageItemRepository.deleteAll();
        personRepository.deleteAll();
        licenseRepository.deleteAll();
    }

    @Test
    public void should_return_passed_user_to_registration() throws Exception {

        PackageItem item = packageItemRepository.findByName("test-item");
        PersonGenerate personGenerate = new PersonGenerate("username", encrypted, "name", "surname",
                "newmail@mail.com", "phone", Collections.singletonList(item), "Warsaw", "02-100",
                "Mickiewicza", 10, "12345678900");

        mvc.perform(post("/open/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(personGenerate)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(personGenerate.getName())));

        Assert.assertNotNull(keycloakService.getUser(personGenerate.getUsername()));
    }
}
