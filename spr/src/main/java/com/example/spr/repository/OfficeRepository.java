package com.example.spr.repository;

import com.example.spr.models.entities.Office;
import org.springframework.data.repository.CrudRepository;

public interface OfficeRepository extends CrudRepository<Office, Long> {

    Office findByNumber(int number);
}
