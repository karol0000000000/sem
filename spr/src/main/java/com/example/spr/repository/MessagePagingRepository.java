package com.example.spr.repository;

import com.example.spr.models.entities.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDateTime;

public interface MessagePagingRepository extends PagingAndSortingRepository<Message, Long> {

    Page<Message> findByDateLessThan(LocalDateTime date, Pageable page);
}
