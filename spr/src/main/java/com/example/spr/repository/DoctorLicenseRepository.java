package com.example.spr.repository;

import com.example.spr.models.entities.Person;
import com.example.spr.models.entities.DoctorLicense;
import com.example.spr.models.entities.License;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DoctorLicenseRepository extends CrudRepository<DoctorLicense, Long> {

    List<DoctorLicense> findByDoctor(Person doctor);
    List<DoctorLicense> findByLicense(License license);
}
