package com.example.spr.repository;

import com.example.spr.models.entities.Person;
import com.example.spr.models.entities.PackageItem;
import com.example.spr.models.entities.PackageItemPatient;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PackageItemPersonRepository extends CrudRepository<PackageItemPatient, Long> {

    PackageItemPatient findByItem(PackageItem item);
    List<PackageItemPatient> findByPatient(Person patient);
}
