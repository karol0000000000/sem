package com.example.spr.repository;

import com.example.spr.models.entities.Person;
import com.example.spr.models.entities.License;
import com.example.spr.models.entities.Service;
import com.example.spr.models.payload_models.ServiceType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

public interface ServiceRepository extends CrudRepository<Service, Long> {
    
    List<Service> findByDoctorAndLicenseAndPatientIdAndTypeAndTimeGreaterThanEqualAndTimeLessThanEqual(
            Person doctor, License license, Long patientId, ServiceType type, LocalDateTime start, LocalDateTime end
    );

    @Query(value = "select distinct DATE(time) from service where doctor_id=:doctorId and license_id=:licenseId and patient_id is null and time > now()", nativeQuery = true)
    List<Date> getDistinctTimeByDoctorIdAndLicenseId(Long doctorId, Long licenseId);
    Long countByPatientIdAndType(Long id, ServiceType type);

    List<Service> findAllByType(ServiceType type);
    Long countByType(ServiceType type);
}
