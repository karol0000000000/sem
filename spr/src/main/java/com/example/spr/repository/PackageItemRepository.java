package com.example.spr.repository;

import com.example.spr.models.entities.PackageItem;
import org.springframework.data.repository.CrudRepository;

public interface PackageItemRepository extends CrudRepository<PackageItem, Long> {

    PackageItem findByName(String name);
    PackageItem findById(long id);
}
