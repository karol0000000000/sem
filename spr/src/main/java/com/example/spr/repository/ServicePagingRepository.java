package com.example.spr.repository;

import com.example.spr.models.entities.Person;
import com.example.spr.models.entities.License;
import com.example.spr.models.entities.Service;
import com.example.spr.models.payload_models.ServiceType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface ServicePagingRepository extends PagingAndSortingRepository<Service, Long> {

    List<Service> findByDoctorAndLicenseAndPatientIdAndTypeAndTimeGreaterThanEqualAndTimeLessThanEqual(
            Person doctor, License license, Long patientId, ServiceType type, LocalDateTime start, LocalDateTime end, Sort sort
    );
    List<Service> findByPatientIdAndType(Long id, ServiceType type, Pageable pageable);

    List<Service> findByType(ServiceType type, Pageable pageable);
}

