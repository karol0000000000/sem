package com.example.spr.repository;

import com.example.spr.models.entities.HomeMessage;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface HomeMessagePagingRepository extends PagingAndSortingRepository<HomeMessage, Long> {
}
