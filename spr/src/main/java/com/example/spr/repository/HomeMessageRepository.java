package com.example.spr.repository;

import com.example.spr.models.entities.HomeMessage;
import org.springframework.data.repository.CrudRepository;

public interface HomeMessageRepository extends CrudRepository<HomeMessage, Long> {
}
