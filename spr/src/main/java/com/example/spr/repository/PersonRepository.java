package com.example.spr.repository;

import com.example.spr.models.entities.Person;
import com.example.spr.models.payload_models.PersonType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {
    Person findByUsername(String username);
    Person findByEmail (String email);
    List<Person> findByType (PersonType type);
    Person findByNameAndSurnameAndType(String name, String surname, PersonType type);
}
