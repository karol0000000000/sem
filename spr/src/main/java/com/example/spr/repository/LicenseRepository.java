package com.example.spr.repository;

import com.example.spr.models.entities.License;
import org.springframework.data.repository.CrudRepository;

public interface LicenseRepository extends CrudRepository<License, Long> {

    License findByName(String name);
    Iterable<License> findByType(String type);
}
