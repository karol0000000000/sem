package com.example.spr.models.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@ApiModel
@Data
@Entity
@NoArgsConstructor
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(required = true)
    private String message;
    @ApiModelProperty(required = true)
    @ManyToOne
    private Person person;
    @ApiModelProperty(required = true)
    private LocalDateTime date;

    public Message(String message, Person person, LocalDateTime date) {
        this.message = message;
        this.person = person;
        this.date = date;
    }
}
