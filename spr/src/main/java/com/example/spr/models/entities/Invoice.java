package com.example.spr.models.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@ApiModel
@Entity
@Data
@NoArgsConstructor
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @ApiModelProperty
    private Person patient;
    @ApiModelProperty
    private LocalDateTime exposure;
    @ApiModelProperty
    private LocalDateTime deadline;
    @ApiModelProperty
    private double fee;
    @ApiModelProperty
    private boolean paid;

    public Invoice(Person patient, LocalDateTime exposure, LocalDateTime deadline, double fee, boolean paid) {
        this.patient = patient;
        this.exposure = exposure;
        this.deadline = deadline;
        this.fee = fee;
        this.paid = paid;
    }
}
