package com.example.spr.models.payload_models;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;

@Data
public class ServiceFilter {

    @ApiModelProperty(required = true)
    private String doctorName;
    @ApiModelProperty(required = true)
    private String doctorSurname;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
    @ApiModelProperty(required = true)
    private LocalDate date;
    @ApiModelProperty(required = true)
    private String licenseName;
    @ApiModelProperty
    private String username;
    @ApiModelProperty
    private Page page;
    @ApiModelProperty
    private ServiceType type;
}
