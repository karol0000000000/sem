package com.example.spr.models.payload_models;

import com.example.spr.models.entities.PackageItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonGenerate {

    private String username;
    private String password;
    private String name;
    private String surname;
    private String email;
    private String phone;
    private List<PackageItem> packageItemList;
    private String city;
    private String postalCode;
    private String street;
    private int houseNumber;
    private int apartmentNumber;
    private String pesel;

    public PersonGenerate(String username, String password, String name, String surname, String email, String phone,
                          List<PackageItem> packageItemList, String city, String postalCode, String street, int houseNumber, String pesel) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.packageItemList = packageItemList;
        this.city = city;
        this.postalCode = postalCode;
        this.street = street;
        this.houseNumber = houseNumber;
        this.pesel = pesel;
    }
}
