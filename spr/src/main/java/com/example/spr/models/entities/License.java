package com.example.spr.models.entities;

import com.example.spr.models.payload_models.LicenseType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@ApiModel
@NoArgsConstructor
@AllArgsConstructor
public class License {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty
    @Enumerated(EnumType.STRING)
    private LicenseType type;

    @ApiModelProperty
    private String name;

    public License(LicenseType type, String name) {
        this.type = type;
        this.name = name;
    }
}
