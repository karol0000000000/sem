package com.example.spr.models.payload_models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@ApiModel
public class ServiceBook {

    @ApiModelProperty(required = true)
    private String doctorName;
    @ApiModelProperty(required = true)
    private String doctorSurname;
    @ApiModelProperty(required = true)
    private LocalDateTime dateTime;
    @ApiModelProperty(required = true)
    private String licenseName;
    @ApiModelProperty
    private String username;
    @NotNull
    @ApiModelProperty
    private boolean wantToBook;
    @ApiModelProperty
    private ServiceType type;
}
