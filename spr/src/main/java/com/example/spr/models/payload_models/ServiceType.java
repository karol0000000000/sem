package com.example.spr.models.payload_models;

public enum ServiceType {

    VISIT("VISIT"),
    TEST("TEST"),
    PROCEDURE("PROCEDURE");

    private ServiceType(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    String name;
}
