package com.example.spr.models.payload_models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Value;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PropertiesDTO {

    String env;
    boolean production;
    String address_frontend;
    String port_frontend;
    String address_websocket_backend;
    String address_backend;
    String port_backend;
    String keycloak_url;
    String keycloak_realm;
    String keycloak_clientId;
    String keycloak_secret;
}
