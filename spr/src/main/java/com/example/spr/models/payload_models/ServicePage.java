package com.example.spr.models.payload_models;

import lombok.Data;

@Data
public class ServicePage {

    private int pageNumber;
    private int size;
    private ServiceType type;
}
