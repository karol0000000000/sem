package com.example.spr.models.entities;

import com.example.spr.models.payload_models.ServiceType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@ApiModel
@Entity
@Data
@NoArgsConstructor
public class Service {

    public Service(Person doctor, LocalDateTime time, License license, Office office, Person patient) {
        this.doctor = doctor;
        this.time = time;
        this.office = office;
        this.patient = patient;
        this.license = license;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @ApiModelProperty(required = true)
    private Person doctor;
    @ApiModelProperty(required = true)
    private LocalDateTime time;
    @ApiModelProperty(required = true)
    @ManyToOne
    private Office office;
    @ApiModelProperty(required = true)
    @ManyToOne
    private License license;
    @ApiModelProperty
    @ManyToOne
    private Person patient;
    @ApiModelProperty
    @Enumerated(EnumType.STRING)
    private ServiceType type;
}
