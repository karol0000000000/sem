package com.example.spr.models.exceptions;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel
public class SpringException {

    @ApiModelProperty(position = 1)
    LocalDateTime timestamp;
    @ApiModelProperty(position = 2)
    int status;
    @ApiModelProperty(position = 3)
    String error;
    @ApiModelProperty(position = 4)
    String message;
    @ApiModelProperty(position = 5)
    String path;
}
