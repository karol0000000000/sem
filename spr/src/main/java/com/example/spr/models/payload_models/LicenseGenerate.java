package com.example.spr.models.payload_models;

import lombok.Data;

@Data
public class LicenseGenerate {

    private String name;
    private LicenseType type;
}
