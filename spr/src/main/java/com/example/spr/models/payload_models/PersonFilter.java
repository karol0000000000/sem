package com.example.spr.models.payload_models;

import lombok.Data;

@Data
public class PersonFilter {

    private String username;

    public PersonFilter() {
    }
    
    public PersonFilter(String username) {
        this.username = username;
    }
}
