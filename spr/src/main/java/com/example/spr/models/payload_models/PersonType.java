package com.example.spr.models.payload_models;

public enum PersonType {

    PATIENT("PATIENT"),
    DOCTOR("DOCTOR"),
    ADMIN("ADMIN");

    private PersonType(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    String name;
}
