package com.example.spr.models.payload_models;

import com.example.spr.models.entities.Person;
import com.example.spr.models.entities.PackageItem;
import lombok.Data;

@Data
public class LicenseFilter {

    private LicenseType type;
    private String name;
    private Person doctor;
    private PackageItem item;
    private String username;
}
