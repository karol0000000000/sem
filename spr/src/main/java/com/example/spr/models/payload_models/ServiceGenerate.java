package com.example.spr.models.payload_models;

import com.example.spr.models.entities.Person;
import com.example.spr.models.entities.License;
import com.example.spr.models.entities.Office;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel
public class ServiceGenerate {
    
    @ApiModelProperty(required = true)
    private Person doctor;
    @ApiModelProperty
    private LocalDateTime startDateTime;
    @ApiModelProperty(required = true)
    private LocalDateTime endDateTime;
    @ApiModelProperty(required = true)
    private int timeSpaceInMinutes;
    @ApiModelProperty(required = true)
    private Office office;
    @ApiModelProperty
    private License license;
    @ApiModelProperty
    private ServiceType type;
}
