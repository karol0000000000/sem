package com.example.spr.models.payload_models;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MessageSave {

    private String message;
    private String username;
    private LocalDateTime dateTime;
}
