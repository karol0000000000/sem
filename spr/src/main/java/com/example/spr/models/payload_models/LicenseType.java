package com.example.spr.models.payload_models;

public enum LicenseType {

    VISIT("VISIT"),
    TEST("TEST"),
    PROCEDURE("PROCEDURE");

    private LicenseType(String name){
        this.name = name;
    }

    String name;
}
