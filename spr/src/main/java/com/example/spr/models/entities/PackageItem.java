package com.example.spr.models.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@ApiModel
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PackageItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty
    private String name;

    @ApiModelProperty
    private int price;

    @ManyToOne
    @ApiModelProperty
    private License license;

    public PackageItem(String name, int price, License license) {
        this.name = name;
        this.price = price;
        this.license = license;
    }
}
