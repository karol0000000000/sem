package com.example.spr.models.payload_models;

import com.example.spr.models.entities.Person;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class InvoiceGenerate {

    @ApiModelProperty
    private LocalDateTime exposure;
    @ApiModelProperty
    private LocalDateTime deadline;
    @ApiModelProperty
    private double fee;
    @ApiModelProperty
    private Person patient;
    @ApiModelProperty
    private boolean paid;
}
