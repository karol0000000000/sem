package com.example.spr.models.payload_models;

import com.example.spr.models.entities.Person;
import lombok.Data;

@Data
public class DoctorLicenseFilter {

    String name;
    Person doctor;
    LicenseType type;
}
