package com.example.spr.models.payload_models;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MessagesAfterDate {

    private LocalDateTime date;
    private Page page;
}
