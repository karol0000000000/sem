package com.example.spr.models.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PackageItemPatient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @ApiModelProperty
    private PackageItem item;

    @ManyToOne
    @ApiModelProperty
    private Person patient;

    public PackageItemPatient(PackageItem item, Person patient) {
        this.item = item;
        this.patient = patient;
    }
}
