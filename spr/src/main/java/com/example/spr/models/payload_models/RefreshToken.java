package com.example.spr.models.payload_models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel
@Data
public class RefreshToken {

    @ApiModelProperty(required = true)
    String username;
}
