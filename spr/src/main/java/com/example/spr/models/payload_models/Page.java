package com.example.spr.models.payload_models;

import com.example.spr.models.entities.Service;
import lombok.Data;

@Data
public class Page {

    private int pageNumber;
    private int size;
}
