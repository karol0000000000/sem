package com.example.spr.models.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@ApiModel
@Entity
@Data
@NoArgsConstructor
public class HomeMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty
    private String name;
    @ApiModelProperty
    private String email;
    @ApiModelProperty
    private String phone;
    @ApiModelProperty
    private String company;
    @ApiModelProperty
    private String message;
    @ApiModelProperty
    private LocalDateTime dateTime;

}
