package com.example.spr.models.payload_models;

import com.example.spr.models.entities.Person;
import com.example.spr.models.entities.License;
import lombok.Data;

@Data
public class DoctorLicenseGenerate {

    private Person doctor;
    private License license;
}
