package com.example.spr.models.entities;

import com.example.spr.models.payload_models.PersonType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@ApiModel
@Data
@Entity
@NoArgsConstructor
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(required = true)
    private String username;
    @ApiModelProperty
    private String name;
    @ApiModelProperty
    private String surname;
    @ApiModelProperty
    private String email;
    @ApiModelProperty
    private String phone;
    @ApiModelProperty
    @Enumerated(EnumType.STRING)
    private PersonType type;
    @ApiModelProperty
    private String city;
    @ApiModelProperty
    private String postalCode;
    @ApiModelProperty
    private String street;
    @ApiModelProperty
    private int houseNumber;
    @ApiModelProperty
    private int apartmentNumber;
    @ApiModelProperty
    private String pesel;

    public Person(String username, String name, String surname, String email, String phone, PersonType type,
                  String city, String postalCode, String street, int houseNumber, int apartmentNumber, String pesel) {
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.type = type;
        this.city = city;
        this.postalCode = postalCode;
        this.street = street;
        this.houseNumber = houseNumber;
        this.apartmentNumber = apartmentNumber;
        this.pesel = pesel;
    }

    public Person(String username, String name, String surname, String email, String phone, PersonType type,
                  String city, String postalCode, String street, int houseNumber, String pesel) {
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.type = type;
        this.city = city;
        this.postalCode = postalCode;
        this.street = street;
        this.houseNumber = houseNumber;
        this.pesel = pesel;
    }
}
