package com.example.spr.models.payload_models;

import lombok.Data;

import java.util.List;

@Data
public class DeleteIdArray {

    private Iterable<Long> ids;
}
