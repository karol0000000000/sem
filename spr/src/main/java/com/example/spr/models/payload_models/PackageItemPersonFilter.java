package com.example.spr.models.payload_models;

import com.example.spr.models.entities.PackageItem;
import lombok.Data;

@Data
public class PackageItemPersonFilter {

    private PackageItem item;
    private String username;
    private LicenseType type;
}
