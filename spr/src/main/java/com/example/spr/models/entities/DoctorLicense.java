package com.example.spr.models.entities;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@ApiModel
@Entity
@Data
@NoArgsConstructor
public class DoctorLicense {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Person doctor;
    @ManyToOne
    private License license;

    public DoctorLicense(Person doctor, License license) {
        this.doctor = doctor;
        this.license = license;
    }
}
