package com.example.spr.models.payload_models;

import lombok.Data;

@Data
public class PersonFilterByEmail {

    private String email;
}
