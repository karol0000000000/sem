package com.example.spr.controllers;

import com.example.spr.models.entities.*;
import com.example.spr.models.exceptions.SpringException;
import com.example.spr.models.payload_models.*;
import com.example.spr.repository.*;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = {"${front.origin}"})
@RestController
@PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
@Slf4j
@RequestMapping("/api")
public class ServiceApi {

    @Autowired
    private ServiceRepository serviceRepository;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private LicenseRepository licenseRepository;
    @Autowired
    private OfficeRepository officeRepository;
    @Autowired
    private ServicePagingRepository servicePagingRepository;

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Service.class, responseContainer = "List")})
    @PostMapping(value = "/services", produces = "application/json")
    public @ResponseBody Iterable<Service> getAllServicesByType(@RequestBody ServiceTypePayload payload) {
        return serviceRepository.findAllByType(payload.getType());
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Service.class)})
    @PostMapping("/services/delete")
    public void deleteServices(@RequestBody DeleteIdArray deleteIdArray) {

        Iterable<Service> allById = serviceRepository.findAllById(deleteIdArray.getIds());
        serviceRepository.deleteAll(allById);
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = void.class)})
    @PostMapping("/services/generate")
    public void generateServices(@RequestBody ServiceGenerate params) {

        LocalDate tempDate = params.getStartDateTime().toLocalDate();
        LocalTime tempTime = params.getStartDateTime().toLocalTime();
        LocalDate endDate = params.getEndDateTime().toLocalDate();
        LocalTime endTime = params.getEndDateTime().toLocalTime();
        List<Service> servicesToSave = new ArrayList<>();

        while (!tempTime.isAfter(endTime) && !tempDate.isAfter(endDate)) {

            Service newV = new Service();
            newV.setDoctor(params.getDoctor());
            newV.setTime(LocalDateTime.of(tempDate, tempTime));
            newV.setLicense(params.getLicense());
            newV.setOffice(params.getOffice());
            newV.setPatient(null);
            newV.setType(params.getType());

            servicesToSave.add(newV);

            tempTime = tempTime.plusMinutes(params.getTimeSpaceInMinutes());
            if (tempTime.equals(endTime) || tempTime.isAfter(endTime)) {
                tempTime = params.getStartDateTime().toLocalTime();
                tempDate = tempDate.plusDays(1);
            }
        }

        serviceRepository.saveAll(servicesToSave);
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Service.class, responseContainer = "List")})
    @PostMapping(value = "/services/filter", produces = "application/json")
    public Iterable<Service> getFilteredServices(@RequestBody ServiceFilter filter) {

        Person d = personRepository.findByNameAndSurnameAndType(filter.getDoctorName(),
                filter.getDoctorSurname(), PersonType.DOCTOR);
        License l = licenseRepository.findByName(filter.getLicenseName());

        if (filter.getDate() != null && filter.getDoctorName()
                != null && filter.getLicenseName() != null) {

            return servicePagingRepository.findByDoctorAndLicenseAndPatientIdAndTypeAndTimeGreaterThanEqualAndTimeLessThanEqual(
                    d, l,
                    null,
                    filter.getType(),
                    LocalDateTime.of(filter.getDate(), LocalTime.of(0, 0)),
                    LocalDateTime.of(filter.getDate(), LocalTime.of(23, 59)),
                    Sort.by("time")
            );

        } else if (filter.getUsername() != null) {

            Pageable custPage = PageRequest.of(filter.getPage().getPageNumber(), filter.getPage().getSize(), Sort.by("time"));
            Person user = personRepository.findByUsername(filter.getUsername());
            return servicePagingRepository.findByPatientIdAndType(user.getId(), filter.getType(), custPage);
        } else return null;
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Long.class)})
    @PostMapping(value = "/service/filter/length", produces = "application/json")
    public @ResponseBody Long countServiceByUsername(@RequestBody ServiceFilter filter) {

        Person user = personRepository.findByUsername(filter.getUsername());
        return serviceRepository.countByPatientIdAndType(user.getId(), filter.getType());
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = boolean.class)})
    @PostMapping(value = "/services/book", produces = "application/json")
    public boolean bookService(@RequestBody ServiceBook serviceBook) {

        Person user = personRepository.findByUsername(serviceBook.getUsername());
        Person d = personRepository.findByNameAndSurnameAndType(serviceBook.getDoctorName(),
                serviceBook.getDoctorSurname(), PersonType.DOCTOR);
        License l = licenseRepository.findByName(serviceBook.getLicenseName());
        Service v;
        if (serviceBook.isWantToBook()) {
            try {
                v = serviceRepository.findByDoctorAndLicenseAndPatientIdAndTypeAndTimeGreaterThanEqualAndTimeLessThanEqual(
                        d, l,
                        null,
                        serviceBook.getType(),
                        serviceBook.getDateTime(), serviceBook.getDateTime()).get(0);
            } catch (Exception e) {
                System.out.println(e);
                return false;
            }
            if (v == null) return false;
            else {
                v.setPatient(user);
                serviceRepository.save(v);
                return true;
            }
        } else {
            try {
                v = serviceRepository.findByDoctorAndLicenseAndPatientIdAndTypeAndTimeGreaterThanEqualAndTimeLessThanEqual(
                        d, l,
                        user.getId(),
                        serviceBook.getType(),
                        serviceBook.getDateTime(), serviceBook.getDateTime()).get(0);
            } catch (Exception e) {
                System.out.println(e);
                return false;
            }
            if (v == null) return false;
            else {
                v.setPatient(null);
                serviceRepository.save(v);
                return true;
            }
        }
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Service.class, responseContainer = "List")})
    @PostMapping(value = "/service/page", produces = "application/json")
    public @ResponseBody Iterable<Service> getServicePage(@RequestBody ServicePage page) {

        Pageable custPage = PageRequest.of(page.getPageNumber(), page.getSize(), Sort.by("time"));
        return servicePagingRepository.findByType(page.getType(), custPage);
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Long.class)})
    @PostMapping(value = "/service/length", produces = "application/json")
    public @ResponseBody Long getServiceLength(@RequestBody ServiceTypePayload payload) {

        return serviceRepository.countByType(payload.getType());
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = LocalDateTime.class, responseContainer = "List")})
    @PostMapping(value = "/service/availability", produces = "application/json")
    public @ResponseBody List<Date> getServiceAvailability(@RequestBody ServiceAvailability payload) {

        return serviceRepository.getDistinctTimeByDoctorIdAndLicenseId(payload.getDoctorId(), payload.getLicenseId());
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Service.class)})
    @PostMapping(value = "/service/set/person", produces = "application/json")
    public @ResponseBody Service setServiceUser(@RequestBody Service service) {

        return serviceRepository.save(service);
    }
}