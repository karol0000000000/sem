package com.example.spr.controllers;

import com.example.spr.models.entities.Person;
import com.example.spr.models.entities.PackageItem;
import com.example.spr.models.entities.PackageItemPatient;
import com.example.spr.models.exceptions.SpringException;
import com.example.spr.models.payload_models.PackageItemPersonFilter;
import com.example.spr.repository.PackageItemPersonRepository;
import com.example.spr.repository.PersonRepository;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = {"${front.origin}"})
@RequestMapping("/api")
public class PackageItemPersonApi {

    @Autowired
    private PackageItemPersonRepository packageItemPersonRepository;

    @Autowired
    private PersonRepository personRepository;

    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = PackageItemPatient.class)})
    @GetMapping(value = "/package-item/person", produces = "application/json")
    public @ResponseBody
    Iterable<PackageItemPatient> getPackagesItemPerson() {
        return packageItemPersonRepository.findAll();
    }

    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = PackageItemPatient.class)})
    @PostMapping(value = "/package-item/person-by-item", produces = "application/json")
    public @ResponseBody
    PackageItemPatient getPackageItemPersonByItem(@RequestBody PackageItemPersonFilter filter) {
        return packageItemPersonRepository.findByItem(filter.getItem());
    }

    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = PackageItemPatient.class)})
    @PostMapping(value = "/package-item/person-by-person", produces = "application/json")
    public @ResponseBody
    Iterable<PackageItem> getPackageItemPersonByPerson(@RequestBody PackageItemPersonFilter filter) {

        Person user = personRepository.findByUsername(filter.getUsername());
        return packageItemPersonRepository.findByPatient(user).stream().map(PackageItemPatient::getItem).collect(Collectors.toList());
    }
}
