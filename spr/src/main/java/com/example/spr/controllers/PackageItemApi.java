package com.example.spr.controllers;

import com.example.spr.models.entities.PackageItem;
import com.example.spr.models.exceptions.SpringException;
import com.example.spr.models.payload_models.PackageItemFilter;
import com.example.spr.repository.PackageItemRepository;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = {"${front.origin}"})
@RequestMapping("/api")
public class PackageItemApi {

    @Autowired
    private PackageItemRepository packageItemRepository;

    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = PackageItem.class)})
    @GetMapping(value = "/open/package-item", produces = "application/json")
    public @ResponseBody Iterable<PackageItem> getPackages() {
        return packageItemRepository.findAll();
    }

    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = PackageItem.class)})
    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
    @PostMapping(value = "/package-item", produces = "application/json")
    public @ResponseBody
    PackageItem getPackageByName(@RequestBody PackageItemFilter packageItemFilter) {
        return packageItemRepository.findByName(packageItemFilter.getName());
    }

    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = PackageItem.class, responseContainer = "List")})
    @GetMapping(value = "/open/package-item/base-packages", produces = "application/json")
    public @ResponseBody Iterable<PackageItem> getBasePackages() {
        List<PackageItem> base = new ArrayList<>();
        base.add(this.packageItemRepository.findByName("Cosmetology consultation"));
        base.add(this.packageItemRepository.findByName("Hot stone massage"));
        base.add(this.packageItemRepository.findByName("Examination of skin surface"));

        return base;
    }
}
