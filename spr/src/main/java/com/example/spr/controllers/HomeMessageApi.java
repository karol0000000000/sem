package com.example.spr.controllers;

import com.example.spr.models.entities.HomeMessage;
import com.example.spr.models.exceptions.SpringException;
import com.example.spr.models.payload_models.Page;
import com.example.spr.repository.HomeMessagePagingRepository;
import com.example.spr.repository.HomeMessageRepository;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"${front.origin}"})
@RequestMapping("/api")
public class HomeMessageApi {

    @Autowired
    private HomeMessageRepository homeMessageRepository;

    @Autowired
    private HomeMessagePagingRepository homeMessagePagingRepository;

    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = HomeMessage.class)})
    @PostMapping(value = "/open/home-message/save", produces = "application/json")
    public @ResponseBody
    HomeMessage saveHomeMessage(@RequestBody HomeMessage message) {

        return homeMessageRepository.save(message);
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = HomeMessage.class, responseContainer = "List")})
    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
    @GetMapping(value = "/home-message", produces = "application/json")
    public @ResponseBody Iterable<HomeMessage> getHomeMessages() {

        return homeMessageRepository.findAll();
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = HomeMessage.class, responseContainer = "List")})
    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
    @PostMapping(value = "/home-message/page", produces = "application/json")
    public @ResponseBody Iterable<HomeMessage> getHomeMessagesPage(@RequestBody Page page) {

        Pageable custPage = PageRequest.of(page.getPageNumber(), page.getSize());
        return homeMessagePagingRepository.findAll(custPage);
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Long.class)})
    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
    @GetMapping(value = "/home-message/length", produces = "application/json")
    public @ResponseBody Long getHomeMessagesLength() {

        return homeMessageRepository.count();
    }
}
