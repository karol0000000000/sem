package com.example.spr.controllers;

import com.example.spr.models.entities.Person;
import com.example.spr.models.entities.DoctorLicense;
import com.example.spr.models.entities.License;
import com.example.spr.models.payload_models.LicenseFilter;
import com.example.spr.models.exceptions.SpringException;
import com.example.spr.models.payload_models.LicenseGenerate;
import com.example.spr.repository.DoctorLicenseRepository;
import com.example.spr.repository.LicenseRepository;
import com.example.spr.repository.PackageItemPersonRepository;
import com.example.spr.repository.PersonRepository;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = {"${front.origin}"})
@PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
@RequestMapping("/api")
public class LicenseApi {

    @Autowired
    private LicenseRepository licenseRepository;

    @Autowired
    private PackageItemPersonRepository packageItemPersonRepository;

    @Autowired
    private DoctorLicenseRepository doctorLicenseRepository;

    @Autowired
    private PersonRepository personRepository;

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = License.class)})
    @PostMapping(value = "/license/filter/name", produces = "application/json")
    public @ResponseBody
    License getLicenseByName(@RequestBody LicenseFilter licenseFilter) {
        return licenseRepository.findByName(licenseFilter.getName());
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = License.class, responseContainer = "List")})
    @PostMapping(value = "/license/filter/type", produces = "application/json")
    public @ResponseBody
    Iterable<License> getLicenseByType(@RequestBody LicenseFilter licenseFilter) {
        return licenseRepository.findByType(licenseFilter.getType().name());
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = License.class)})
    @PostMapping(value = "/license/generate", produces = "application/json")
    public @ResponseBody
    License generateLicense(@RequestBody LicenseGenerate param) {
        return licenseRepository.save(new License(param.getType(), param.getName()));
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = License.class, responseContainer = "List")})
    @PostMapping(value = "/license/by/doctor/type", produces = "application/json")
    public @ResponseBody
    Iterable<License> getLicenseByDoctorAndType(@RequestBody LicenseFilter licenseFilter) {

        return doctorLicenseRepository.findByDoctor(licenseFilter.getDoctor()).stream().map(DoctorLicense::getLicense)
                .filter(l -> l.getType().equals(licenseFilter.getType())).collect(Collectors.toList());
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = License.class, responseContainer = "List")})
    @PostMapping(value = "/license/by/doctor", produces = "application/json")
    public @ResponseBody
    Iterable<License> getLicenseByDoctor(@RequestBody Person doctor) {

        return doctorLicenseRepository.findByDoctor(doctor).stream().map(DoctorLicense::getLicense).collect(Collectors.toList());
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = License.class, responseContainer = "List")})
    @PostMapping(value = "/license/by/person/type", produces = "application/json")
    public @ResponseBody
    Iterable<License> getLicenseByUsernameAndType(@RequestBody LicenseFilter licenseFilter) {

        Person person = personRepository.findByUsername(licenseFilter.getUsername());
        return packageItemPersonRepository.findByPatient(person).stream().map(p -> p.getItem()
                .getLicense()).filter(l -> l.getType().equals(licenseFilter.getType())).collect(Collectors.toList());
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = License.class, responseContainer = "List")})

    @PostMapping(value = "/license/possible", produces = "application/json")
    public @ResponseBody
    Iterable<License> getPossibleLicenseForDoctor(@RequestBody Person doctor) {

        List<License> current = doctorLicenseRepository.findByDoctor(doctor).stream().map(DoctorLicense::getLicense).collect(Collectors.toList());
        List<License> all = (List<License>) licenseRepository.findAll();

        return all.stream().filter(f -> !current.contains(f)).collect(Collectors.toList());
    }
}
