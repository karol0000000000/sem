package com.example.spr.controllers;

import com.example.spr.models.entities.Person;
import com.example.spr.models.entities.Invoice;
import com.example.spr.models.exceptions.SpringException;
import com.example.spr.models.payload_models.InvoiceGenerate;
import com.example.spr.models.payload_models.PersonFilter;
import com.example.spr.repository.InvoiceRepository;
import com.example.spr.repository.PersonRepository;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"${front.origin}"})
@PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
@RequestMapping("/api")
public class InvoiceApi {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Invoice.class)})
    @PostMapping(value = "/invoice/generate", produces = "application/json")
    public @ResponseBody
    Invoice generateInvoice(@RequestBody InvoiceGenerate invoiceGenerate) {

        return invoiceRepository.save(new Invoice(invoiceGenerate.getPatient(), invoiceGenerate.getExposure(), invoiceGenerate.getDeadline(),
                invoiceGenerate.getFee(), invoiceGenerate.isPaid()));
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Invoice.class, responseContainer = "List")})
    @PostMapping(value = "/invoice/filter", produces = "application/json")
    public @ResponseBody Iterable<Invoice> getInvoices(@RequestBody PersonFilter user) {

        Person user1 = personRepository.findByUsername(user.getUsername());
        return invoiceRepository.findByPatientId(user1.getId());
    }
}
