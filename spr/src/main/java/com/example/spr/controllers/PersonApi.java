package com.example.spr.controllers;

import com.example.spr.models.entities.Person;
import com.example.spr.models.entities.DoctorLicense;
import com.example.spr.models.entities.License;
import com.example.spr.models.exceptions.SpringException;
import com.example.spr.models.payload_models.DoctorLicenseFilter;
import com.example.spr.models.payload_models.PersonFilter;
import com.example.spr.models.payload_models.PersonFilterByEmail;
import com.example.spr.models.payload_models.PersonType;
import com.example.spr.repository.DoctorLicenseRepository;
import com.example.spr.repository.LicenseRepository;
import com.example.spr.repository.PersonRepository;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = {"${front.origin}"})
@RequestMapping("/api")
public class PersonApi {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private LicenseRepository licenseRepository;
    @Autowired
    private DoctorLicenseRepository doctorLicenseRepository;

    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Person.class)})
    @PostMapping(value = "/open/person/by/username", produces = "application/json")
    public @ResponseBody
    Person getPersonByUsername(@RequestBody PersonFilter personFilter) {
        return personRepository.findByUsername(personFilter.getUsername());
    }

    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Person.class)})
    @PostMapping(value = "/open/person/by/email", produces = "application/json")
    public @ResponseBody
    Person getPersonByEmail(@RequestBody PersonFilterByEmail personFilter) {
        return personRepository.findByEmail(personFilter.getEmail());
    }

    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Person.class, responseContainer = "List")})
    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
    @GetMapping(value = "/person", produces = "application/json")
    public @ResponseBody Iterable<Person> getAllPersons() {
        return personRepository.findAll();
    }

    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Person.class, responseContainer = "List")})
    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
    @GetMapping(value = "/patient", produces = "application/json")
    public @ResponseBody Iterable<Person> getAllPatient() {
        return personRepository.findByType(PersonType.PATIENT);
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Person.class, responseContainer = "List")})
    @PostMapping(value = "/doctor/by/license", produces = "application/json")
    public @ResponseBody
    Iterable<Person> getDoctorByLicense(@RequestBody DoctorLicenseFilter filter) {

        License s = licenseRepository.findByName(filter.getName());
        return doctorLicenseRepository.findByLicense(s).stream().map(DoctorLicense::getDoctor).collect(Collectors.toList());
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Person.class, responseContainer = "List")})
    @GetMapping(value = "/doctor", produces = "application/json")
    public @ResponseBody
    Iterable<Person> getAllDoctors() {
        return personRepository.findByType(PersonType.DOCTOR);
    }
}
