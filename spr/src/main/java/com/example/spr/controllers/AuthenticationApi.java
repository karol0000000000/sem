package com.example.spr.controllers;

import com.example.spr.models.entities.Person;
import com.example.spr.models.entities.PackageItemPatient;
import com.example.spr.models.exceptions.SpringException;
import com.example.spr.models.payload_models.PersonGenerate;
import com.example.spr.models.payload_models.PersonType;
import com.example.spr.repository.PackageItemRepository;
import com.example.spr.repository.PackageItemPersonRepository;
import com.example.spr.repository.PersonRepository;
import com.example.spr.service.PassService;
import com.example.spr.service.KeycloakService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = {"${front.origin}"})
@Slf4j
@RequestMapping("/api")
public class AuthenticationApi {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private KeycloakService keycloakService;

    @Autowired
    private PassService passService;

    @Autowired
    private PackageItemPersonRepository packageItemPersonRepository;

    @Autowired
    private PackageItemRepository packageItemRepository;

    @PostMapping("/open/register")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Person.class)})
    ResponseEntity<Person> register(@RequestBody PersonGenerate user) {

        user.setPassword(passService.decodePass(user.getPassword()));
        Person person = new Person(user.getUsername().toLowerCase(), user.getName(), user.getSurname(), user.getEmail(),
                user.getPhone(), PersonType.PATIENT, user.getCity(), user.getPostalCode(), user.getStreet(),
                user.getHouseNumber(), user.getPesel());
        person.setApartmentNumber(Math.max(user.getApartmentNumber(), 0));
        Person saved = personRepository.save(person);
        int status = keycloakService.registerUser(user);

        if(status >= 200 && status <= 204){
            try {
                List<PackageItemPatient> packages = new ArrayList<>();
                user.getPackageItemList().forEach(p -> {
                    packages.add(new PackageItemPatient(p, saved));
                });
                this.packageItemPersonRepository.saveAll(packages);
            }catch(Exception e) {
                keycloakService.deleteUser(saved.getUsername());
                personRepository.delete(saved);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return ResponseEntity.ok().body(saved);
        }else{
            personRepository.delete(saved);
            log.error("Keycloak registration failed");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
