package com.example.spr.controllers;

import com.example.spr.models.entities.DoctorLicense;
import com.example.spr.models.entities.License;
import com.example.spr.models.exceptions.SpringException;
import com.example.spr.models.payload_models.DoctorLicenseGenerate;
import com.example.spr.repository.DoctorLicenseRepository;
import com.example.spr.repository.LicenseRepository;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"${front.origin}"})
@PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
@RequestMapping("/api")
public class DoctorLicenseApi {

    @Autowired
    private DoctorLicenseRepository doctorLicenseRepository;
    @Autowired
    private LicenseRepository licenseRepository;

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = License.class, responseContainer = "List")})

    @PostMapping(value = "/doctor-license/generate", produces = "application/json")
    public @ResponseBody
    DoctorLicense addNewDoctorLicense(@RequestBody DoctorLicenseGenerate param) {

        return doctorLicenseRepository.save(new DoctorLicense(param.getDoctor(), param.getLicense()));
    }
}