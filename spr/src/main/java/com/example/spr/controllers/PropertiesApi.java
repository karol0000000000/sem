package com.example.spr.controllers;

import com.example.spr.models.exceptions.SpringException;
import com.example.spr.service.PassService;
import com.example.spr.service.Properties;
import com.example.spr.models.payload_models.PropertiesDTO;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"${front.origin}"})
@RequestMapping("/api")
public class PropertiesApi {

    @Autowired
    private Properties properties;
    @Autowired
    private PassService passService;

    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = PropertiesDTO.class)})
    @GetMapping(value = "/open/properties", produces = "application/json")
    public @ResponseBody
    PropertiesDTO getProperties() {
        return new PropertiesDTO(
                properties.getEnv(),
                properties.isProduction(),
                properties.getAddress_frontend(),
                properties.getPort_frontend(),
                properties.getAddress_websocket_backend(),
                properties.getAddress_backend(),
                properties.getPort_backend(),
                properties.getKeycloak_url(),
                properties.getKeycloak_realm(),
                properties.getKeycloak_clientId(),
                passService.encrypt(properties.getKeycloak_secret())
        );
    }
}
