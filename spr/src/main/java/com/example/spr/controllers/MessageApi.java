package com.example.spr.controllers;

import com.example.spr.models.entities.Person;
import com.example.spr.models.entities.Message;
import com.example.spr.models.exceptions.SpringException;
import com.example.spr.models.payload_models.MessageSave;
import com.example.spr.models.payload_models.MessagesAfterDate;
import com.example.spr.models.payload_models.Page;
import com.example.spr.repository.MessagePagingRepository;
import com.example.spr.repository.MessageRepository;
import com.example.spr.repository.PersonRepository;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = {"${front.origin}"})
@RequestMapping("/api")
public class MessageApi {

    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private MessagePagingRepository messagePagingRepository;
    @Autowired
    private PersonRepository personRepository;

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Message.class, responseContainer = "List")})
    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
    @GetMapping(value = "/message", produces = "application/json")
    public @ResponseBody Iterable<Message> getMessages() {
        return messageRepository.findAll();
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Message.class, responseContainer = "List")})
    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
    @PostMapping(value = "/message/page", produces = "application/json")
    public @ResponseBody Iterable<Message> getMessagesByPage(@RequestBody Page page) {

        Pageable custPage = PageRequest.of(page.getPageNumber(), page.getSize(), Sort.by("date").descending());
        return Lists.reverse(messagePagingRepository.findAll(custPage).stream().collect(Collectors.toList()));
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Message.class, responseContainer = "List")})
    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
    @PostMapping(value = "/message/after/date", produces = "application/json")
    public @ResponseBody Iterable<Message> getMessagesAfterDate(@RequestBody MessagesAfterDate messagesAfterDate) {

        Pageable custPage = PageRequest.of(messagesAfterDate.getPage().getPageNumber(), messagesAfterDate.getPage().getSize(), Sort.by("date").descending());
        return messagePagingRepository.findByDateLessThan(messagesAfterDate.getDate(), custPage);
    }

    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Long.class)})
    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_patient')")
    @GetMapping(value = "/message/length", produces = "application/json")
    public @ResponseBody Long getMessagesLength() {

        return messageRepository.count();
    }

    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error", response = SpringException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = SpringException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SpringException.class),
            @ApiResponse(code = 404, message = "Service not found", response = SpringException.class),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = Message.class)})
    @MessageMapping(value = "/server-receiver")
    @SendTo(value = "/topic/server-broadcaster")
    public @ResponseBody Message saveMessage(@RequestBody MessageSave messageSave) {

        Person user = personRepository.findByUsername(messageSave.getUsername());
        return messageRepository.save(new Message(messageSave.getMessage(), user, messageSave.getDateTime()));
    }
}
