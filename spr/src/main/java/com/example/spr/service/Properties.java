package com.example.spr.service;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Data
public class Properties {

    @Value("${env}")
    String env;
    @Value(("${fe.production}"))
    boolean production;
    @Value("${front.origin}")
    String address_frontend;
    @Value("${fe.port_frontend}")
    String port_frontend;
    @Value("${fe.address_websocket_backend}")
    String address_websocket_backend;
    @Value("${fe.address_backend}")
    String address_backend;
    @Value("${fe.port_backend}")
    String port_backend;
    @Value("${keycloak.auth-server-url}")
    String keycloak_url;
    @Value("${keycloak.realm}")
    String keycloak_realm;
    @Value("${fe.client_id}")
    String keycloak_clientId;
    @Value("${secret}")
    String keycloak_secret;
}
