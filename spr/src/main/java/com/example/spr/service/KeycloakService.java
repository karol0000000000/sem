package com.example.spr.service;

import com.example.spr.models.payload_models.PersonGenerate;
import lombok.extern.slf4j.Slf4j;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;

@Service
@Slf4j
public class KeycloakService {

    @Value("${keycloak.auth-server-url}")
    private String serverUrl;
    @Value("${master.realm}")
    private String masterRealm;
    @Value("${username_keycloak}")
    private String username;
    @Value("${password_keycloak}")
    private String password;
    @Value("${master.client.id}")
    private String masterClient;
    @Value("${keycloak.realm}")
    private String realm;
    @Value("${patient.role}")
    private String patientRole;

    private Keycloak keycloakInstance = null;

    public int registerUser(PersonGenerate daoUser) {

        Keycloak keycloak = this.getKeycloakInstance();

        RealmResource realmResource = keycloak.realm(this.realm);
        UsersResource userRessource = realmResource.users();

        CredentialRepresentation passwordCred = this.getCredentials(daoUser.getPassword());
        UserRepresentation user = new UserRepresentation();

        user.setUsername(daoUser.getUsername());
        user.setCredentials(Collections.singletonList(passwordCred));
        user.setEnabled(true);
        user.setEmail(daoUser.getEmail());
        user.setFirstName(daoUser.getName());
        user.setLastName(daoUser.getSurname());
        Response res = null;

        try {
            res = keycloak.realm(this.realm).users().create(user);
            String userId = res.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");
            RoleRepresentation role = null;

            role = realmResource.roles().get(this.patientRole).toRepresentation();
            userRessource.get(userId).roles().realmLevel().add(Collections.singletonList(role));
            log.info("Keycloak user was created");
        } catch (Exception e) {
            log.error(e.getMessage());
            return res != null ? res.getStatus() : 500;
        }

        return res.getStatus();
    }

    private Keycloak getKeycloakInstance() {

        if (this.keycloakInstance == null) {
            this.keycloakInstance = KeycloakBuilder.builder()
                    .serverUrl(this.serverUrl)
                    .realm(this.masterRealm)
                    .username(this.username)
                    .password(this.password)
                    .clientId(this.masterClient)
                    .resteasyClient(
                            new ResteasyClientBuilder()
                                    .connectionPoolSize(10).build()
                    ).build();
        }

        return this.keycloakInstance;
    }

    private CredentialRepresentation getCredentials(String password) {

        CredentialRepresentation passwordCred = new CredentialRepresentation();
        passwordCred.setTemporary(false);
        passwordCred.setType(CredentialRepresentation.PASSWORD);
        passwordCred.setValue(password);

        return passwordCred;
    }

    public int deleteUser(String username) {

        Keycloak keycloak = getKeycloakInstance();
        Response res = null;
        try {
            List<UserRepresentation> search = keycloak.realm(realm).users().search(username);
            res = keycloak.realm(realm).users().delete(search.get(0).getId());
            return res != null ? res.getStatus() : 500;
        } catch (Exception e) {
            log.error(e.getMessage());
            return res != null ? res.getStatus() : 500;
        }
    }

    public List<UserRepresentation> getUser(String username) {

        Keycloak keycloak = getKeycloakInstance();
        try {
            return keycloak.realm(realm).users().search(username);
        } catch (Exception e) {
            return null;
        }
    }
}
