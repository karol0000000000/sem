--license

insert into license (name, type) values ('Examination of skin moisture level', 'TEST');
insert into license (name, type) values ('Examination of oily skin', 'TEST');
insert into license (name, type) values ('Examination of skin smoothness', 'TEST');
insert into license (name, type) values ('Examination of skin color', 'TEST');
insert into license (name, type) values ('Examination of skin sensitivity level', 'TEST');
insert into license (name, type) values ('Examination of skin thickness and stratum corneum', 'TEST');
insert into license (name, type) values ('Examination of skin elasticity', 'TEST');
insert into license (name, type) values ('Examination of skin reaction', 'TEST');
insert into license (name, type) values ('Examination of skin surface', 'TEST');
insert into license (name, type) values ('Cosmetology consultation', 'VISIT');
insert into license (name, type) values ('Pre-procedure consultation', 'VISIT');
insert into license (name, type) values ('Removal of spider veins', 'PROCEDURE');
insert into license (name, type) values ('Laser hair removal', 'PROCEDURE');
insert into license (name, type) values ('Hot stone massage', 'PROCEDURE');
insert into license (name, type) values ('Waxing', 'PROCEDURE');
insert into license (name, type) values ('Tattoo removal', 'PROCEDURE');
insert into license (name, type) values ('Laser discoloration removal', 'PROCEDURE');
insert into license (name, type) values ('Upper eyelid lift', 'PROCEDURE');
insert into license (name, type) values ('Laser closing of blood vessels', 'PROCEDURE');

--packageItem

insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Examination of skin moisture level') select 'Examination of skin moisture level', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Examination of oily skin') select 'Examination of oily skin', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Examination of skin smoothness') select 'Examination of skin smoothness', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Examination of skin color') select 'Examination of skin color', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Examination of skin sensitivity level') select 'Examination of skin sensitivity level', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Examination of skin thickness and stratum corneum') select 'Examination of skin thickness and stratum corneum', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Examination of skin elasticity') select 'Examination of skin elasticity', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Examination of skin reaction') select 'Examination of skin reaction', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Examination of skin surface') select 'Examination of skin surface', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Cosmetology consultation') select 'Cosmetology consultation', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Pre-procedure consultation') select 'Pre-procedure consultation', 2, t1.id from t1;
insert into package_item(name, price, license_id) WITH t1 as (select id from license where name='Removal of spider veins') select 'Removal of spider veins', 2, t1.id from t1;
insert into package_item(name, price, license_id) WITH t1 as (select id from license where name='Laser hair removal') select 'Laser hair removal', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Hot stone massage') select 'Hot stone massage', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Waxing') select 'Waxing', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Tattoo removal') select 'Tattoo removal', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Laser discoloration removal') select 'Laser discoloration removal', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Upper eyelid lift') select 'Upper eyelid lift', 2, t1.id from t1;
insert into package_item (name, price, license_id) WITH t1 as (select id from license where name='Laser closing of blood vessels') select 'Laser closing of blood vessels', 2, t1.id from t1;

--doctor

insert into person (username, name, surname, email, type, city, "postal_code", street, "house_number", "apartment_number", pesel)
            values ('rwierzbicki', 'Rafał', 'Wierzbicki', 'rafal.wierzbicki@med.pl', 'DOCTOR', 'Warsaw', '00-001', 'Medyczna', 1, 1, '12345678900');
insert into person (username, name, surname, email, type, city, "postal_code", street, "house_number", "apartment_number", pesel)
            values ('mostrowska', 'Matylda', 'Ostrowska', 'matylda.ostrowska@med.pl', 'DOCTOR', 'Warsaw', '00-001', 'Medyczna', 2, 2, '12345678901');
insert into person (username, name, surname, email, type, city, "postal_code", street, "house_number", "apartment_number", pesel)
            values ('kpawlowska', 'Karolina', 'Pawłowska', 'karolina.pawlowska@med.pl', 'DOCTOR', 'Warsaw', '00-001', 'Medyczna', 3, 3, '12345678902');

--office

insert into office (number) values (1);
insert into office (number) values (2);
insert into office (number) values (3);
insert into office (number) values (4);
insert into office (number) values (5);

--add_admin

insert into person (email, name, phone, surname, username, type, city, "postal_code", street, "house_number", "apartment_number", pesel)
            values ('admin@mail.com', 'admin_name', '', 'admin_surname', 'admin', 'ADMIN', 'Warsaw', '00-001', 'Medyczna', 4, 4, '12345678903')
