CREATE TABLE person (
    id bigserial NOT NULL PRIMARY KEY,
    email character varying(255),
    name character varying(255),
    phone character varying(255),
    surname character varying(255),
    username character varying(255),
    type character varying(255),
    city character varying(255),
    postal_code character varying(6),
    street character varying(255),
    house_number integer,
    apartment_number integer,
    pesel character varying(11)
);

CREATE TABLE doctor_license (
    id bigserial NOT NULL PRIMARY KEY,
    doctor_id bigint,
    license_id bigint
);

CREATE TABLE home_message (
    id bigserial NOT NULL PRIMARY KEY,
    company character varying(255),
    date_time timestamp without time zone,
    email character varying(255),
    message character varying(255),
    name character varying(255),
    phone character varying(255)
);

CREATE TABLE invoice (
    id bigserial NOT NULL PRIMARY KEY,
    deadline timestamp without time zone,
    exposure timestamp without time zone,
    fee double precision NOT NULL,
    paid boolean NOT NULL,
    patient_id bigint
);

CREATE TABLE license (
    id bigserial NOT NULL PRIMARY KEY,
    name character varying(255),
    type character varying(255)
);

CREATE TABLE message (
    id bigserial NOT NULL PRIMARY KEY,
    date timestamp without time zone,
    message character varying(255),
    person_id bigint
);

CREATE TABLE office (
    id bigserial NOT NULL PRIMARY KEY,
    number integer
);

CREATE TABLE package_item (
    id bigserial NOT NULL PRIMARY KEY,
    name character varying(255),
    price integer,
    license_id bigint
);

CREATE TABLE package_item_patient (
    id bigserial NOT NULL PRIMARY KEY,
    item_id bigint,
    patient_id bigint
);

CREATE TABLE service (
    id bigserial NOT NULL PRIMARY KEY,
    "time" timestamp without time zone,
    doctor_id bigint,
    license_id bigint,
    office_id bigint,
    patient_id bigint,
    type character varying(255)
);



