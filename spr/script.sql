--packageItem

insert into packageItem (name, price) values ('BASIC', 10);
insert into packageItem (name, price) values ('ADVANCED', 20);
insert into packageItem (name, price) values ('PROFESSIONAL', 25);


--visitType

insert into visitType (name) values ('Internist');
insert into visitType (name) values ('Cardiologist');
insert into visitType (name) values ('Radiologist');

--doctor

insert into doctor (name, surname) values ('Bryan', 'Williams');
insert into doctor (name, surname) values ('Isabell', 'Davis');
insert into doctor (name, surname) values ('Dorothy', 'Lopez');

--office

insert into office (number) values (1);
insert into office (number) values (2);
insert into office (number) values (3);

--test_type

insert into test_type (type) values ('USG');
insert into test_type (type) values ('RTG');
insert into test_type (type) values ('EKG');

--add_admin

insert into dao_user (email, name, phone, surname, username) values ('admin@mail.com', 'admin_name', '', 'admin_surname', 'admin')
