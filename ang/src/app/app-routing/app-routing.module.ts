import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'
import { RegisterComponent } from '../register/register.component';
import { HomeComponent } from '../home/home.component';
import { VisitsComponent } from '../patient/visits/visits.component';
import { TestsComponent } from '../patient/tests/tests.component';
import { MessengerComponent } from '../messenger/messenger.component';
import { HomePatientComponent } from '../patient/home-patient/home-patient.component';
import { HomeAdminComponent } from '../admin/home-admin/home-admin.component';
import { ErrorComponent } from '../error/error.component';
import { KeycloakAppAuthGuardService } from '../service/keycloak/keycloak-app-auth-guard.service';
import { HomeMessagesComponent } from '../admin/home-messages/home-messages.component';
import { VisitManageComponent } from '../admin/visit-manage/visit-manage.component';
import { TestManageComponent } from '../admin/test-manage/test-manage.component';
import { ProcedureManageComponent } from '../admin/procedure-manage/procedure-manage.component';
import { ProceduresComponent } from '../patient/procedures/procedures.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'appointments', component: VisitsComponent, canActivate: [KeycloakAppAuthGuardService], data: {roles: ['patient']} },
  { path: 'check-ups', component: TestsComponent, canActivate: [KeycloakAppAuthGuardService], data: {roles: ['patient']} },
  { path: 'procedures', component: ProceduresComponent, canActivate: [KeycloakAppAuthGuardService], data: {roles: ['patient']} },
  { path: 'messenger', component: MessengerComponent, canActivate: [KeycloakAppAuthGuardService], data: {roles: ['patient', 'admin']} },
  { path: 'register/:package', component: RegisterComponent },
  { path: 'patient', component: HomePatientComponent, canActivate: [KeycloakAppAuthGuardService], data: {roles: ['patient']}},
  { path: 'admin', component: HomeAdminComponent, canActivate: [KeycloakAppAuthGuardService], data: {roles: ['admin']}},
  { path: 'message', component: HomeMessagesComponent, canActivate: [KeycloakAppAuthGuardService], data: {roles: ['admin']}},
  { path: 'visit-manage', component: VisitManageComponent, canActivate: [KeycloakAppAuthGuardService], data: {roles: ['admin']}},
  { path: 'test-manage', component: TestManageComponent, canActivate: [KeycloakAppAuthGuardService], data: {roles: ['admin']}},
  { path: 'procedure-manage', component: ProcedureManageComponent, canActivate: [KeycloakAppAuthGuardService], data: {roles: ['admin']}},
  { path: '**', component: ErrorComponent},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    RouterModule
  ],
  exports: [
    RouterModule
  ],
  providers: [KeycloakAppAuthGuardService]
})
export class AppRoutingModule { }
