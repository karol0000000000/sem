import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class MockHttp extends HttpClient {

    get<T>(url, options): Observable<T> {return of()}
    post<T>(url, body, options): Observable<T> {return of()}
    put<T>(url, body, options): Observable<T> {return of()}
    delete<T>(url, options): Observable<T> {return of()}
}