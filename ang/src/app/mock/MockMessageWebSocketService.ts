import { MessageWebSocketService } from '../service/message-web-socket.service';
import { Observable, Subject } from 'rxjs';

export class MockMessageWebSocketService extends MessageWebSocketService {
    connect() {}

    getSubject(): Observable<any> {
        return new Subject().asObservable();
    }

    disconnect() {}
};