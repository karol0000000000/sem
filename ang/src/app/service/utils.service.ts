import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(private router: Router) { }

  reloadLayout() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigateByUrl(this.router.url);
  }

  getISOFromDateWithOffset(date: Date): string {
    let d: Date = date;
    d.setHours(date.getHours() - (date.getTimezoneOffset() / 60));
    return d.toISOString();
  }
}
