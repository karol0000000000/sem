import { Injectable, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { Properties } from 'src/app/models/models';
import { HttpService } from '../http.service';

@Injectable({
  providedIn: 'root'
})
export class KeycloakApiService {

  env: Properties = null;
  constructor(private keycloak: KeycloakService, private service: HttpService) {
    this.service.getPropertiesObservable().subscribe(props => this.env = props);
  }

  login() {
    this.keycloak.login();
  }

  logout() {
    this.keycloak.logout(this.env['address_frontend'] + ':' + this.env['port_frontend']);
  }

  async isUserLoggedIn() {
    return await this.keycloak.isLoggedIn();
  }

  getRoles(): string[] {
    return this.keycloak.getUserRoles();
  }

  getUsername(): string {
    return this.keycloak.getUsername();
  }

  register() {
    this.keycloak.register();
  }
}
