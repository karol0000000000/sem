import { TestBed } from '@angular/core/testing';

import { KeycloakApiService } from './keycloak-api.service';
import { KeycloakAngularModule } from 'keycloak-angular';

describe('KeycloakApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [KeycloakAngularModule]
  }));

  it('should be created', () => {
    const service: KeycloakApiService = TestBed.get(KeycloakApiService);
    expect(service).toBeTruthy();
  });
});
