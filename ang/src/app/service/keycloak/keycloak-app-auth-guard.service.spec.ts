import { TestBed } from '@angular/core/testing';

import { KeycloakAppAuthGuardService } from './keycloak-app-auth-guard.service';
import { Router } from '@angular/router';
import { KeycloakAngularModule } from 'keycloak-angular';

describe('KeycloakAppAuthGuardService', () => {

  const routerSpy = jasmine.createSpyObj('Router', ['navigate', 'navigateByUrl']);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [KeycloakAngularModule],
    providers: [{provide: Router, useValue: routerSpy}]
  }));

  it('should be created', () => {
    const service: KeycloakAppAuthGuardService = TestBed.get(KeycloakAppAuthGuardService);
    expect(service).toBeTruthy();
  });
});
