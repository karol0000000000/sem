import { TestBed } from '@angular/core/testing';

import { UtilsService } from './utils.service';
import { Router } from '@angular/router';

describe('UtilsService', () => {
  const routerSpy = jasmine.createSpyObj('Router', ['navigate', 'navigateByUrl']);
  beforeEach(() => TestBed.configureTestingModule({
    providers: [{provide: Router, useValue: routerSpy}]
  }));

  it('should be created', () => {
    const service: UtilsService = TestBed.get(UtilsService);
    expect(service).toBeTruthy();
  });
});
