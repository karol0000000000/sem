import { TestBed } from '@angular/core/testing';

import { HttpService } from './http.service';
import { HttpClientModule, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { MockHttp } from '../mock/MockHttp';
import { PackageItem, License, LicenseType, Office } from '../models/models';
import { of, throwError } from 'rxjs';
import {
  HttpClientTestingModule,
  HttpTestingController,
  TestRequest
} from '@angular/common/http/testing';

describe('HttpService', () => {

  describe('HttpServiceTestedWithModule', () => {

    let serviceMock: HttpService, httpMock: HttpTestingController;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });

      serviceMock = TestBed.get(HttpService);
      httpMock = TestBed.get(HttpTestingController);
    });

    describe('should get offices', () => {

      describe('should offices download successfully', () => {

        let offices: Office[];

        beforeEach(() => {
          offices = [
            { id: 1, number: 1 },
            { id: 2, number: 2 },
          ];
        });

        it('should return offices', () => {
          serviceMock.getAllOffices().subscribe(offices => {
            expect(offices.length).toEqual(2, 'count of offices');
            expect(offices).toEqual(offices, 'contain offices');
          });
        });

        afterEach(() => {
          const req: TestRequest = httpMock.expectOne('http://localhost:8090/office');
          expect(req.request.method).toEqual('GET');
          req.flush(offices);
        })
      });

      describe('should fail on download offices', () => {

        it('should return error message', () => {
          serviceMock.getAllOffices().subscribe({
            error: (e) => {
              expect(e.error.type).toEqual('Service unavailable');
            }
          });
        });

        afterEach(() => {
          const req: TestRequest = httpMock.expectOne('http://localhost:8090/office');
          expect(req.request.method).toEqual('GET');
          req.error(new ErrorEvent('Service unavailable'));
        })
      });
    });
  });

  describe('HttpServiceWithProvider', () => {
    let service: HttpService, http;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientModule],
        providers: [{ provide: HttpClient, useClass: MockHttp }]
      })
      service = TestBed.get(HttpService);
      http = TestBed.get(HttpClient);
    });

    it('should be created', () => {
      service = TestBed.get(HttpService);
      expect(service).toBeTruthy();
    });

    describe('should get base packages', () => {

      describe('should base packages download successfully', () => {
        let packages: PackageItem[];
        let license: License;

        beforeEach(() => {
          license = { id: 1, type: LicenseType.TEST, name: 'testLicense' };
          packages = [
            { id: 1, name: 'test1', price: 20, license: license }
          ];
          spyOn(http, 'get').and.returnValue(of(packages));
        });

        it('should make request', () => {

          service.getBasePackage();
          expect(http.get).toHaveBeenCalledWith('http://localhost:8090/open/package-item/base-packages');
        });

        it('should return items', () => {

          service.getBasePackage().subscribe(items => {
            expect(items.length).toEqual(1, 'count of items');
            expect(items).toEqual(packages, 'contain packages');
          });
        });
      });

      describe('should fail on download base packages', () => {

        beforeEach(() => {
          spyOn(http, 'get').and.returnValue(throwError("Service unavailable"));
        });

        it('should return error message', () => {
          service.getBasePackage().subscribe({
            error: (errorMessage) => {
              expect(errorMessage).toEqual('Service unavailable');
            }
          });
        });
      });
    });
  });

});