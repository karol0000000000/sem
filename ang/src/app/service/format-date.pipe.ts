import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {

    let date: Date = new Date(value);
    return (args[0] ? this.check(date.getFullYear()) + "." + this.check((date.getMonth()) + 1) + "." + this.check(date.getDate()) + ", " +
    this.check(date.getHours()) + ":" + this.check(date.getMinutes()) : this.check(date.getFullYear()) + "." + this.check((date.getMonth()) + 1) + "." + this.check(date.getDate()));
  }

  //check if zero needed
  check(num: number) {
    if(num < 10)return "0"+num;
    else return num;
  }

}
