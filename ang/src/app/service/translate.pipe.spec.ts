import { TranslatePipe } from './translate.pipe';
import { TranslationService } from './translation.service';
import { inject } from '@angular/core/testing';
import { setCurrLanguage } from '../app.module';

describe('TranslatePipe', () => {
  it('create an instance', inject([TranslationService], (translationService) => {
    setCurrLanguage(translationService);
    const pipe = new TranslatePipe(translationService);
    expect(pipe).toBeTruthy();
  }));
});
