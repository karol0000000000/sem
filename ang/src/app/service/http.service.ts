import * as core from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import * as CryptoJS from 'crypto-js';
import {
  Person, Invoice,
  Message, MessageSave,
  Office,
  InvoiceGenerate,
  HomeMessage,
  Page,
  idsToDelete,
  MessagesAfterDate,
  PackageItem,
  PackageItemFilter,
  License,
  LicenseFilter,
  DoctorLicenseGenerate,
  DoctorLicense,
  LicenseGenerate,
  PersonGenerate,
  PackageItemPersonFilter,
  DoctorLicenseFilter,
  ServiceFilter,
  Service,
  ServiceBook,
  ServiceTypePayload,
  ServiceGenerate,
  ServicePage,
  ServiceAvailability,
  PropertiesDTO,
  Properties,
} from '../models/models';
import { environment } from 'src/environments/environment';
import { concatMap } from 'rxjs/operators';

@core.Injectable({
  providedIn: 'root'
})
export class HttpService {

  env: Properties = null;
  initEnv = environment;
  constructor(private httpClient: HttpClient) {
    this.getPropertiesObservable().subscribe(props => this.env = props);
  }
  readonly path: string = this.initEnv['env'] === 'development' ?
    this.initEnv['address_backend'] + ':' + this.initEnv['port_backend'] + "/api" :
    this.initEnv['address_backend'];

  private loadProperties(): Observable<Properties> {

    if (this.env === null) {

      return this.getProperties().pipe(concatMap(
        props => {
          this.env = {
            env: props.env,
            production: props.production,
            address_frontend: props.address_frontend,
            port_frontend: props.port_frontend,
            address_websocket_backend: props.address_websocket_backend,
            address_backend: props.address_backend,
            port_backend: props.port_backend,
            keycloakConfig: {
              url: props.keycloak_url,
              realm: props.keycloak_realm,
              clientId: props.keycloak_clientId,
              credentials: {
                secret: CryptoJS.AES.decrypt(props.keycloak_secret, CryptoJS.enc.Base64.parse(this.initEnv.secret), {
                  mode: CryptoJS.mode.ECB,
                  padding: CryptoJS.pad.Pkcs7
                }).toString(CryptoJS.enc.Utf8)
              }
            }
          }
          return of(this.env);
        }
      ))
    } else return of(this.env);
  }


  public getPropertiesPromise(): Promise<Properties> {

    return new Promise((resolve, reject) => {
      this.loadProperties().subscribe(props => resolve(props));
    });
  }

  public getPropertiesObservable(): Observable<Properties> {

    return this.loadProperties();
  }

  register(personGenerate: PersonGenerate): Observable<Person> {
    return this.httpClient.post<Person>(this.path + '/open/register', personGenerate)
  }

  getFilteredServices(param: ServiceFilter): Observable<Service[]> {
    return this.httpClient.post<Service[]>(this.path + '/services/filter', param);
  }

  getServiceLengthByUsername(param: ServiceFilter): Observable<number> {
    return this.httpClient.post<number>(this.path + '/service/filter/length', param);
  }

  bookService(param: ServiceBook): Observable<boolean> {
    return this.httpClient.post<boolean>(this.path + '/services/book', param);
  }

  getServices(param: ServiceTypePayload): Observable<Service[]> {
    return this.httpClient.post<Service[]>(this.path + '/services', param);
  }

  deleteService(ids: idsToDelete): Observable<void> {
    return this.httpClient.post<void>(this.path + '/services/delete', ids);
  }

  getPersonByUsername(username: string): Observable<Person> {
    return this.httpClient.post<Person>(this.path + '/open/person/by/username', { username });
  }

  getPersonByEmail(email: string): Observable<Person> {
    return this.httpClient.post<Person>(this.path + '/open/person/by/email', { email });
  }

  getInvoices(username: string): Observable<Invoice[]> {
    return this.httpClient.post<Invoice[]>(this.path + '/invoice/filter', { username });
  }

  getPackages(): Observable<PackageItem[]> {
    return this.httpClient.get<PackageItem[]>(this.path + "/open/package-item");
  }

  getProperties(): Observable<PropertiesDTO> {
    return this.httpClient.get<PropertiesDTO>(this.path + "/open/properties");
  }

  getPackageItemByName(param: PackageItemFilter): Observable<PackageItem> {
    return this.httpClient.post<PackageItem>(this.path + "/package-item", param);
  }

  getPackageItemByPerson(param: PackageItemPersonFilter): Observable<PackageItem[]> {
    return this.httpClient.post<PackageItem[]>(this.path + "/package-item/person-by-person", param);
  }

  getDoctorsByLicense(param: DoctorLicenseFilter): Observable<Person[]> {
    return this.httpClient.post<Person[]>(this.path + "/doctor/by/license", param);
  }

  getLicenseByType(param: LicenseFilter): Observable<License[]> {
    return this.httpClient.post<License[]>(this.path + '/license/filter/type', param);
  }

  getLicenseByDoctorAndType(param: LicenseFilter): Observable<License[]> {
    return this.httpClient.post<License[]>(this.path + '/license/by/doctor/type', param);
  }

  getLicenseByUsernameAndType(param: LicenseFilter): Observable<License[]> {
    return this.httpClient.post<License[]>(this.path + '/license/by/person/type', param);
  }

  getLicenseByName(param: LicenseFilter): Observable<License[]> {
    return this.httpClient.post<License[]>(this.path + '/license/filter/name', param);
  }

  getAllMessages(): Observable<Message[]> {
    return this.httpClient.get<Message[]>(this.path + "/message");
  }

  getAllDoctors(): Observable<Person[]> {
    return this.httpClient.get<Person[]>(this.path + '/doctor');
  }

  getAllOffices(): Observable<Office[]> {
    return this.httpClient.get<Office[]>(this.path + '/office');
  }

  getLicensesByDoctor(param: Person): Observable<License[]> {
    return this.httpClient.post<License[]>(this.path + '/license/by/doctor', param);
  }

  generateServices(param: ServiceGenerate): Observable<void> {
    return this.httpClient.post<void>(this.path + '/services/generate', param);
  }

  getPossibleLicensesForDoctor(doctor: Person): Observable<License[]> {
    return this.httpClient.post<License[]>(this.path + '/license/possible', doctor);
  }

  generateDoctorLicense(param: DoctorLicenseGenerate): Observable<DoctorLicense> {
    return this.httpClient.post<DoctorLicense>(this.path + '/doctor-license/generate', param);
  }

  generateLicense(param: LicenseGenerate): Observable<License> {
    return this.httpClient.post<License>(this.path + '/license/generate', param);
  }

  getAllUsers(): Observable<Person[]> {
    return this.httpClient.get<Person[]>(this.path + '/person');
  }

  getAllPatients(): Observable<Person[]> {
    return this.httpClient.get<Person[]>(this.path + '/patient');
  }

  generateInvoice(param: InvoiceGenerate): Observable<Invoice> {
    return this.httpClient.post<Invoice>(this.path + '/invoice/generate', param);
  }

  saveHomeMessage(param: HomeMessage): Observable<HomeMessage> {
    return this.httpClient.post<HomeMessage>(this.path + '/open/home-message/save', param);
  }

  getAllHomeMessages(): Observable<HomeMessage[]> {
    return this.httpClient.get<HomeMessage[]>(this.path + '/home-message');
  }

  getAllHomeMessageByPage(page: Page): Observable<HomeMessage[]> {
    return this.httpClient.post<HomeMessage[]>(this.path + '/home-message/page', page);
  }

  getHomeMessageLength(): Observable<number> {
    return this.httpClient.get<number>(this.path + '/home-message/length');
  }

  getAllServicesByPage(page: ServicePage): Observable<Service[]> {
    return this.httpClient.post<Service[]>(this.path + '/service/page', page);
  }

  getServicesLength(param: ServiceTypePayload): Observable<number> {
    return this.httpClient.post<number>(this.path + '/service/length', param);
  }

  setUserForService(service: Service): Observable<Service> {
    return this.httpClient.post<Service>(this.path + '/service/set/person', service);
  }

  getAllMessagesByPage(page: Page): Observable<Message[]> {
    return this.httpClient.post<Message[]>(this.path + '/message/page', page);
  }

  getMessagesLength(): Observable<number> {
    return this.httpClient.get<number>(this.path + '/message/length');
  }

  getMessagesAfterDate(param: MessagesAfterDate): Observable<Message[]> {
    return this.httpClient.post<Message[]>(this.path + '/message/after/date', param);
  }

  getBasePackage(): Observable<PackageItem[]> {
    return this.httpClient.get<PackageItem[]>(this.path + '/open/package-item/base-packages');
  }

  getServiceAvailability(payload: ServiceAvailability): Observable<Date[]> {
    return this.httpClient.post<Date[]>(this.path + "/service/availability", payload);
  }

  /**
  * Send message without websocket
  * @deprecated
  */
  saveMessage(params: MessageSave): Observable<Message> {
    return this.httpClient.post<Message>(this.path + "/message", params);
  }
}
