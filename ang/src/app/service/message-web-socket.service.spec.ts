import { TestBed } from '@angular/core/testing';

import { MessageWebSocketService } from './message-web-socket.service';
import { CustomMaterialModule } from '../core/material.module';

describe('MessageWebSocketService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [CustomMaterialModule],
    providers: [MessageWebSocketService]
  }));

  it('should be created', () => {
    const service: MessageWebSocketService = TestBed.get(MessageWebSocketService);
    expect(service).toBeTruthy();
  });
});
