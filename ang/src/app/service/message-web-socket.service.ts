import { StompService, StompConfig } from "@stomp/ng2-stompjs";
import { Message } from "@stomp/stompjs";
import { Observable, Subject } from "rxjs";
import { MessageSave, Properties} from '../models/models';
import { MatSnackBar } from '@angular/material';
import { TranslationService } from './translation.service';
import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable()
export class MessageWebSocketService {
  private messages: Observable<Message>;
  private stompService: StompService;
  private subject: Subject<Message> = new Subject();
  serverErr = this.translationService.translate('serverError');
  env: Properties = null;
  WEBSOCKET_URL: string = null;

  stompConfig: StompConfig = null;

  constructor(private snackBar: MatSnackBar, 
    private translationService: TranslationService, 
    private Service: HttpService) {
      this.Service.getPropertiesObservable().subscribe(props => {
        this.env = props;
        this.WEBSOCKET_URL = this.env['env'] === 'development' ? 
        this.env['address_websocket_backend'] + ":" + this.env['port_backend'] + "/ws/open/ws" :
        this.env['address_websocket_backend'] + "/open/ws";
        
        this.stompConfig = {
          url: this.WEBSOCKET_URL,
          headers: {
            // 'Authorization': sessionStorage.getItem("token")
          },
          heartbeat_in: 0,
          heartbeat_out: 20000,
          reconnect_delay: 5000,
          debug: false
        };
      })
  }

  public connect() {

    // Create Stomp Service
    this.stompService = new StompService(this.stompConfig);
    // Connect to a Stream
    this.messages = this.stompService.subscribe("/topic/server-broadcaster");
    this.stream().subscribe((message) => {
      this.subject.next(message);
    }, err => {
      console.log(err);
      this.snackBar.open(this.serverErr, 'OK');
    })
  }

  public disconnect() {
    this.stompService.disconnect();
  }

  private stream(): Observable<Message> {
    return this.messages;
  }

  public send(message: MessageSave) {
    return this.stompService.publish("/server-receiver", JSON.stringify(message));
  }

  public getSubject(): Observable<Message> {
    return this.subject.asObservable();
  }

}
