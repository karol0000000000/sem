import { Injectable } from '@angular/core';
import { Properties } from '../models/models';
import { HttpService } from './http.service';

export class TranslationSet {
  language: string
  values: {
    [key: string]: string
  } = {}
}

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  env: Properties = null;
  languages = {
    'en_EN': 'English',
    'pl_PL': 'Polski'
  };
  language = '';

  constructor(private service: HttpService) {
    this.service.getPropertiesObservable().subscribe(props => this.env = props);
  }

  dictionary: { [key: string]: TranslationSet } = {
    en_EN: {
      language: 'en_EN',
      values: {
        login: 'Login',
        createAccount: 'Create an Account',
        username: 'Username',
        password: 'Password',
        register: 'Register',
        signUp: 'Sign up',
        add: 'Add',
        logout: 'Logout',
        delete: 'Delete',
        create: 'Create',
        visits: 'Appointments',
        tests: 'Medical check-ups',
        procedures: 'Medical procedures',
        messenger: 'Messenger',
        doctor: 'Doctor',
        specialisation: 'Specialisation',
        type: 'Type',
        time: 'Time',
        office: 'Office',
        save: 'Save',
        close: 'Close',
        chooseDoctor: 'Choose doctor',
        chooseLicense: 'Choose license',
        chooseOffice: 'Choose office',
        chooseDate: 'Choose date',
        chooseStartDate: 'Choose start date',
        chooseEndDate: 'Choose end date',
        chooseTime: 'Choose time',
        chooseStartTime: 'Choose start time',
        chooseEndTime: 'Choose end time',
        chooseTimeSpace: 'Timespaces in minutes',
        chooseUser: 'Choose user',
        chooseExposure: 'Choose exposure date',
        chooseDeadline: 'Choose deadline date',
        typeFee: 'Type fee',
        newVisit: 'New appointment',
        newProcedure: 'New procedure',
        newTest: 'New medical check-up',
        cancel: 'Cancel',
        filter: 'Filter',
        bookVisit: 'Visit was booked',
        cancelVisit: 'Visit was canceled',
        bookProcedure: 'Procedure was booked',
        cancelProcedure: 'Procedure was canceled',
        bookTest: 'Medical check-up was booked',
        cancelTest: 'Medical check-up was canceled',
        name: 'Name',
        surname: 'Surname',
        city: 'City',
        postalCode: 'Postal code',
        street: 'Street',
        houseNumber: 'House number',
        apartmentNumber: 'Apartment number',
        pesel: 'Pesel',
        confirmPass: 'Retype password',
        email: 'Email',
        phone: 'Phone',
        plan: 'Choose plan',
        plan1: 'Plan',
        basic: 'Basic',
        day: 'Day',
        utility: 'Utility',
        advanced: 'Advanced',
        professional: 'Professional',
        userRequired: 'Username is required',
        nameRequired: 'Name is required',
        surnameRequired: 'Surname is required',
        passRequired: 'Password is required',
        passMinLength: 'At least 8 characters',
        confirmPassRequired: 'Retype password',
        passNotMatch: 'Passwords do not match',
        emailRequired: 'Email is required',
        emailIncorrect: 'Email is incorrect',
        packageRequired: 'Package is required',
        cityRequired: 'City is required',
        postalCodeRequired: 'Postal code is required',
        streetRequired: 'Street is required',
        houseNumberRequired: 'House number is required',
        peselRequired: 'Pesel is required',
        phoneIncorrect: 'Phone is incorrect',
        postalCodeIncorrect: 'Postal code is incorrect',
        houseNumberIncorrect: 'House number is incorrect',
        appartmentNumberIncorrect: 'Appartment number is incorrect',
        terms: 'I accept the terms and conditions',
        peselIncorrect: 'Pesel is incorrect',
        welcome: 'Welcome',
        read: 'Read about us',
        buy: 'Take advantage of the offer',
        treatments: 'Professional treatments',
        experience: 'Experience',
        protection: 'Protection',
        qualifications: 'Qualifications',
        service: 'Our Service',
        read1: 'Read about us',
        more: 'More',
        meet: 'Meet our doctors',
        pricingPlans: 'Our pricing plans',
        buyNow: 'Take advantage of the offer now',
        write: 'Write to us',
        company: 'Company',
        message: 'Message',
        contact: 'Contact',
        address: 'Warsaw, Kwiatowa 1, Poland',
        phoneNr: '+48 123456789',
        ourEmail: 'mail@mail.com',
        available: 'Username not available',
        availableEmail: 'Email not available',
        noAppointments: 'No appointments',
        noProcedures: 'No procedures',
        noAppointmentsDate: 'No appointments on this date',
        noProceduresDate: 'No procedures on this date',
        noCheckUps: 'No medical check-ups',
        noCheckUpsDate: 'No medical check-ups on this date',
        currency: 'PLN',
        yourAppointments: 'Your appointments',
        yourProcedures: 'Your procedures',
        yourCheckUps: 'Your medical check-ups',
        doctorName1: 'Rafał Wierzbicki',
        doctorName2: 'Matylda Ostrowska',
        doctorName3: 'Karolina Pawłowska',
        serverError: 'Something goes wrong with server. Please try to restart page or login again',
        noInformation: 'No information',
        change: 'Change',
        pay: 'Pay',
        nearestInvoice: 'Nearest invoice',
        exposure: 'Exposure date',
        deadline: 'Deadline for payment',
        status: 'Status',
        paid: 'Paid',
        unpaid: 'Unpaid',
        fee: 'Fee',
        noInvoices: 'No invoices',
        noMessages: 'No messages',
        user: 'User',
        date: 'Date',
        messages: 'Messages',
        messageRequired: 'Message is required',
        notSupportSocket: 'It seems that your browser do not support web socket. Update it or try another one',
        notFound: 'Not found',
        backToHome: 'Back to homepage',
        adminPage: 'Admin page',
        generateVisit: 'Generating appointments',
        generateProcedure: 'Generating procedures',
        generateTest: 'Generating medical check-ups',
        generate: 'Generate',
        utilities: 'Utilities',
        visitsGenerated: 'Appointments were generated',
        proceduresGenerated: 'Procedures were generated',
        testsGenerated: 'Medical check-ups were generated',
        chooseType: 'Choose type',
        chooseSpec: 'Choose specialisation',
        scrollDown: 'Scroll down on message',
        newDoctorSpec: 'New specialisation for doctor',
        doctorSpecGenerated: 'Specialisation for doctor was generated',
        noLicenseForDoctor: 'No available licenses for this doctor',
        newDoctorLicense: 'New license for doctor',
        doctorLicenseGenerated: 'License for doctor was generated',
        noTypeForDoctor: 'No available check-up types for this doctor',
        newTestType: 'New check-up type',
        newTestTypeGenerated: 'New check-up type was generated',
        newInvoice: 'New invoice',
        newInvoiceGenerated: 'New invoice was generated',
        account: 'Account',
        invoice: 'Invoices',
        userRegistered: 'Registration finished successfully',
        homeMessageSent: 'Message was sent',
        manageVisit: 'Manage appointments',
        manageProcedure: 'Manage procedures',
        manageTest: 'Manage check-ups',
        noUser: 'No user',
        incorrectUser: 'Incorrect user',
        visitsRemoved: 'Appointments were removed',
        visitRemoved: 'Appointment was removed',
        visitChanged: 'Appointment was changed',
        proceduresRemoved: 'Procedures were removed',
        procedureRemoved: 'Procedure was removed',
        procedureChanged: 'Procedure was changed',
        removeSelected: 'Remove selected',
        testsRemoved: 'Medical check-ups were removed',
        testRemoved: 'Medical check-up was removed',
        testChanged: 'Medical check-up was changed',
        noChanges: 'No changes',
        itemsPerPageLabel: 'Items per page:',
        nextPageLabel: 'Next page',
        previousPageLabel: 'Previous page',
        firstPageLabel: 'First page',
        lastPageLabel: 'Last page',
        own: 'Own',
        ownLabel1: '2 appointments types',
        ownLabel2: '9 check-ups types',
        ownLabel3: '8 procedures types',
        packageGenerated: 'New package was added',
        packagesGenerated: 'New packages were added',
        packageHave: 'You have already have this package',
        newPackage: 'New package',
        choosePackage: 'Choose package',
        all: 'All',
        packageRemoved: 'Package was removed',
        packagesRemoved: 'Packages were removed',
        packages: 'Packages',
        noLicenseVisitDoctor: 'Lack of licenses of type visit for this doctor',
        noLicenseProcedureDoctor: 'Lack of licenses of procedure visit for this doctor',
        noLicenseTestDoctor: 'Lack of licenses of type test for this doctor',
        license: 'License',
        noDoctorLicense: 'Lack of doctor for this license',
        chooseLicenseType: 'Choose license type',
        ALL: 'All',
        TEST: 'Check-ups',
        VISIT: 'Appointments',
        PROCEDURE: 'Procedures',
        allFromType: 'All from type',
        selected: 'Selected',
        allPossibleAdded: 'All package which you do not have from this type were added',
        procedure: 'Type of procedure',
        chooseProcedure: 'Choose type of procedure',
        visitPackage: 'Appointments packages',
        testPackage: 'Check-ups packages',
        procedurePackage: 'Procedures packages',
        'Examination of skin moisture level': 'Examination of skin moisture level',
        'Examination of oily skin': 'Examination of oily skin',
        'Examination of skin smoothness': 'Examination of skin smoothness',
        'Examination of skin color': 'Examination of skin color',
        'Examination of skin sensitivity level': 'Examination of skin sensitivity level',
        'Examination of skin thickness and stratum corneum': 'Examination of skin thickness and stratum corneum',
        'Examination of skin elasticity': 'Examination of skin elasticity',
        'Examination of skin reaction': 'Examination of skin reaction',
        'Examination of skin surface' : 'Examination of skin surface' ,
        'Cosmetology consultation': 'Cosmetology consultation',
        'Pre-procedure consultation': 'Pre-procedure consultation',
        'Removal of spider veins': 'Removal of spider veins',
        'Laser hair removal': 'Laser hair removal',
        'Hot stone massage': 'Hot stone massage',
        'Waxing': 'Waxing',
        'Tattoo removal': 'Tattoo removal',
        'Laser discoloration removal': 'Laser discoloration removal',
        'Upper eyelid lift': 'Upper eyelid lift',
        'Laser closing of blood vessels': 'Laser closing of blood vessels',
        address1: 'Address',
        noDates: 'No visits to this type and this doctor',
        onlyLetters: 'Only letters are possible',
        mandatory: 'Mandatory fields',
        allowedSigns: 'Allowed signs: ',
        nameIncorrect: 'Name is incorrect',
        surnameIncorrect: 'Surname is incorrect',
        cityIncorrect: 'City is incorrect',
        streetIncorrect: 'Street is incorrect',
        ownPlanRule: 'In case of own plan you must choose at least one package',
        endDateTimeErr: 'End date and time must be after start date and time'
      }
    },
    pl_PL: {
      language: 'pl_PL',
      values: {
        login: 'Logowanie',
        createAccount: 'Stwórz konto',
        username: 'Nazwa użytkownika',
        password: 'Hasło',
        register: 'Rejestracja',
        signUp: 'Zarejestruj się',
        add: 'Dodaj',
        logout: 'Wyloguj się',
        delete: 'Usuń',
        create: 'Stwórz',
        visits: 'Wizyty',
        tests: 'Badania',
        procedures: 'Zabiegi',
        messenger: 'Komunikator',
        doctor: 'Doctor',
        specialisation: 'Specializacja',
        type: 'Typ',
        time: 'Czas',
        office: 'Gabinet',
        save: 'Zapisz',
        close: 'Zamknij',
        chooseDoctor: 'Wybierz doktora',
        chooseLicense: 'Wybierz licencje',
        chooseOffice: 'Wybierz gabinet',
        chooseDate: 'Wybierz datę',
        chooseStartDate: 'Wybierz początkową datę',
        chooseEndDate: 'Wybierz końcową datę',
        chooseTime: 'Wybierz czas',
        chooseStartTime: 'Wybierz początkowy czas',
        chooseEndTime: 'Wybierz końcowy czas',
        chooseTimeSpace: 'Odstępy czasu w minutach',
        chooseUser: 'Wybierz użytkownika',
        chooseExposure: 'Wybierz datę wystawienia',
        chooseDeadline: 'Wybierz ostateczny termin',
        typeFee: 'Wpisz wysokość opłaty',
        newVisit: 'Nowa wizyta',
        newProcedure: 'Nowy zabieg',
        newTest: 'Nowe badanie',
        cancel: 'Anuluj',
        filter: 'Filtruj',
        bookVisit: 'Wizyta została zarezerwowana',
        cancelVisit: 'Wizyta została anulowana',
        bookProcedure: 'Zabieg został zarezerwowany',
        cancelProcedure: 'Zabieg został anulowany',
        bookTest: 'Badanie zostało zarezerwowane',
        cancelTest: 'Badanie zostało anulowane',
        name: 'Imię',
        surname: 'Nazwisko',
        city: 'Miasto',
        postalCode: 'Kod pocztowy',
        street: 'Ulica',
        houseNumber: 'Numer domu',
        apartmentNumber: 'Numer mieszkania',
        pesel: 'Pesel',
        confirmPass: 'Powtórz hasło',
        email: 'Email',
        phone: 'Telefon',
        plan: 'Wybierz plan',
        plan1: 'Plan',
        basic: 'Podstawowy',
        day: 'Dzień',
        utility: 'Udogodność',
        advanced: 'Zaawansowany',
        professional: 'Profesjonalny',
        userRequired: 'Nazwa użytkownika jest wymagana',
        nameRequired: 'Imię jest wymagane',
        surnameRequired: 'Nazwisko jest wymagane',
        passRequired: 'Hasło jest wymagane',
        passMinLength: 'Przynajmniej 8 znaków',
        confirmPassRequired: 'Powtórz hasło',
        passNotMatch: 'Hasła nie zgadzają się',
        emailRequired: 'Email jest wymagany',
        emailIncorrect: 'Email jest niepoprawny',
        packageRequired: 'Pakiet jest wymagany',
        cityRequired: 'Miasto jest wymagane',
        postalCodeRequired: 'Kod pocztowy jest wymagany',
        streetRequired: 'Ulica jest wymagana',
        houseNumberRequired: 'Numer domu jest wymagany',
        peselRequired: 'Pesel jest wymagany',
        phoneIncorrect: 'Telefon jest niepoprawny',
        postalCodeIncorrect: 'Kod pocztowy jest niepoprawny',
        houseNumberIncorrect: 'Numer domu jest niepoprawny',
        appartmentNumberIncorrect: 'Numer mieszkania jest niepoprawny',
        terms: 'Akceptuję regulamin',
        peselIncorrect: 'Pesel jest niepoprawny',
        welcome: 'Witamy',
        read: 'Przeczytaj o nas',
        buy: 'Skorzystaj z oferty',
        treatments: 'Profesjonalne leczenie',
        experience: 'Doświadczenie',
        protection: 'Ochrona',
        qualifications: 'Kwalifikacje',
        service: 'Nasze usługi',
        read1: 'Przeczytaj o nas',
        more: 'Więcej',
        meet: 'Poznaj naszych lekarzy',
        pricingPlans: 'Nasze oferty',
        buyNow: 'Skorzytaj teraz',
        write: 'Napisz do nas',
        company: 'Firma',
        message: 'Wiadomość',
        contact: 'Kontakt',
        address: 'Warszawa, Kwiatowa 1, Polska',
        phoneNr: '+48 123456789',
        ourEmail: 'mail@mail.com',
        available: 'Nazwa użytownika nie jest dostępna',
        availableEmail: 'Email nie jest dostępny',
        noAppointments: 'Brak wizyt',
        noAppointmentsDate: 'Brak wizyt pod wskazaną datą',
        noProcedures: 'Brak zabiegów',
        noProceduresDate: 'Brak zabiegów pod wskazaną datą',
        noCheckUps: 'Brak badań',
        noCheckUpsDate: 'Brak badań pod wskazaną datą',
        currency: 'PLN',
        yourAppointments: 'Twoje wizyty',
        yourProcedures: 'Twoje zabiegi',
        yourCheckUps: 'Twoje badania',
        doctorName1: 'Rafał Wierzbicki',
        doctorName2: 'Matylda Ostrowska',
        doctorName3: 'Karolina Pawłowska',
        serverError: 'Nieoczekiwany błąd serwera. Spróbuj odświeżyć stronę lub zaloguj się ponownie',
        noInformation: 'Brak informacji',
        change: 'Zmień',
        pay: 'Zapłać',
        nearestInvoice: 'Najbliższa płatność',
        exposure: 'Data wystawienia',
        deadline: 'Termin zapłaty',
        status: 'Status',
        paid: 'Opłacona',
        unpaid: 'Nieopłacona',
        fee: 'Opłata',
        noInvoices: 'Brak faktur',
        noMessages: 'Brak wiadomości',
        user: 'Użytkownik',
        date: 'Data',
        messages: 'Wiadomości',
        messageRequired: 'Wiadomość jest wymagana',
        notSupportSocket: 'Wydaje się, że Twoja przeglądarka nie obsługe web socket\'ów. Zaaktualizuj ją lub spróbuj w innej',
        notFound: 'Nie znaleziono',
        backToHome: 'Wróć do strony głównej',
        adminPage: 'Panel administratora',
        generateVisit: 'Generowanie wizyt',
        generateProcedure: 'Generowanie zabiegów',
        generateTest: 'Generowanie badań',
        generate: 'Generuj',
        utilities: 'Narzędzia',
        visitsGenerated: 'Wizyty zostały wygenerowane',
        proceduresGenerated: 'Zabiegi zostały wygenerowane',
        testsGenerated: 'Badania zostały wygenerowane',
        chooseType: 'Wybierz typ',
        chooseSpec: 'Wybierz specializację',
        scrollDown: 'Przewiń w dół w momencie pojawienia się nowej wiadomości',
        newDoctorSpec: 'Nowa specializacja dla doktora',
        doctorSpecGenerated: 'Specializacja dla doktora została wygenerowana',
        noLicenseForDoctor: 'Brak dostępnych licencji dla tego doktora',
        newDoctorLicense: 'Nowa licencja dla doktora',
        doctorLicenseGenerated: 'Nowa licencja dla doktora została wygenerowana',
        noTypeForDoctor: 'Brak dostępnych typów badań dla tego doktora',
        newTestType: 'Nowy typ badań',
        newTestTypeGenerated: 'Nowy typ badań został wygenerowany',
        newInvoice: 'Nowa faktura',
        newInvoiceGenerated: 'Nowa faktura została wygenerowana',
        account: 'Konto',
        invoice: 'Faktury',
        userRegistered: 'Rejestracja zakończyła się pomyślnie',
        homeMessageSent: 'Wiadomość została wysłana',
        manageVisit: 'Zarządzaj wizytami',
        manageProcedure: 'Zarządzaj zabiegami',
        manageTest: 'Zarządzaj badaniami',
        noUser: 'Brak użytkownika',
        incorrectUser: 'Niepoprawny użytkownik',
        visitsRemoved: 'Wizyty zostały usunięte',
        visitRemoved: 'Wizyta została usunięta',
        removeSelected: 'Usuń zaznaczone',
        visitChanged: 'Wizyta została zmieniona',
        testsRemoved: 'Badania zostały usunięte',
        testRemoved: 'Badanie zostało usunięte',
        testChanged: 'Badanie zostało zmienione',
        noChanges: 'Brak zmian',
        itemsPerPageLabel: 'Elementów na stronie:',
        nextPageLabel: 'Następna strona',
        previousPageLabel: 'Poprzednia strona',
        firstPageLabel: 'Pierwsza strona',
        lastPageLabel: 'Ostatnia strona',
        own: 'Własny',
        ownLabel1: '2 pakiety wizyt',
        ownLabel2: '9 pakietów badań',
        ownLabel3: '8 pakietów zabiegów',
        packageGenerated: 'Nowy pakiet zastał dodany',
        packagesGenerated: 'Nowe pakiety zostały dodane',
        packageHave: 'Posiadasz już ten pakiet',
        newPackage: 'Nowy pakiet',
        choosePackage: 'Wybierz pakiet',
        all: 'Wszystkie',
        packageRemoved: 'Pakiet został usunięty',
        packagesRemoved: 'Pakiety zostały usunięte',
        packages: 'Pakiety',
        noLicenseVisitDoctor: 'Brak licencji o typie wizyta dla tego doktora',
        noLicenseProcedureDoctor: 'Brak licencji o typie zabieg dla tego doktora',
        noLicenseTestDoctor: 'Brak licencji o typie test dla tego doctora',
        license: 'Licencja',
        noDoctorLicense: 'Brak doctora z tą licencją',
        chooseLicenseType: 'Wybierz typ licencji',
        ALL: 'Wszystkie',
        TEST: 'Badania',
        VISIT: 'Wizyty',
        PROCEDURE: 'Zabiegi',
        allFromType: 'Wszystkie z typu',
        selected: 'Wybrany',
        allPossibleAdded: 'Wszystkie pakiety, które nie posiadałeś/aś z tego typu zostały dodane',
        proceduresRemoved: 'Zabiegi zostały usunięte',
        procedureRemoved: 'Zabieg został usunięty',
        procedureChanged: 'Zabieg został zmieniony',
        procedure: 'Typ zabiegu',
        chooseProcedure: 'Wybierz typ zabiegu',
        visitPackage: 'Pakiety wizyt',
        testPackage: 'Pakiety badań',
        procedurePackage: 'Pakiety zabiegów',
        'Examination of skin moisture level': 'Badanie poziomu nawilżenia skóry',
        'Examination of oily skin': 'Badanie przetłuszczenia skóry',
        'Examination of skin smoothness': 'Badanie gładkości skóry',
        'Examination of skin color': 'Badanie zabarwienia skóry',
        'Examination of skin sensitivity level': 'Badanie poziomu wrażliwości skóry',
        'Examination of skin thickness and stratum corneum': 'Badanie grubości skóry i warstwy rogowej',
        'Examination of skin elasticity': 'Badanie elastyczności skóry',
        'Examination of skin reaction': 'Badanie odczynu skóry',
        'Examination of skin surface' : 'Badanie powierzchni skóry' ,
        'Cosmetology consultation': 'Konsultacja kosmetologiczna',
        'Pre-procedure consultation': 'Konsultacja przedzabiegowa',
        'Removal of spider veins': 'Usuwanie pajączków',
        'Laser hair removal': 'Depilacja laserowa',
        'Hot stone massage': 'Masaż ciepłymi kamieniami',
        'Waxing': 'Depilacja woskiem',
        'Tattoo removal': 'Usuwanie tatuażu',
        'Laser discoloration removal': 'Laserowe usuwanie przebarwień',
        'Upper eyelid lift': 'Lifting powieki górnej',
        'Laser closing of blood vessels': 'Laserowe zamykanie naczynek',
        address1: 'Adres',
        noDates: 'Brak wizyt dla tego typu i tego doktora',
        onlyLetters: 'Tylko litery są dozwolone',
        mandatory: 'Pola obowiązkowe',
        allowedSigns: 'Dozwolone znaki: ',
        nameIncorrect: 'Imię jest niepoprawne',
        surnameIncorrect: 'Nazwisko jest niepoprawne',
        cityIncorrect: 'Miasto jest niepoprawne',
        streetIncorrect: 'Ulica jest niepoprawna',
        ownPlanRule: 'W przypadku planu własnego musisz wybrać przynajmniej jeden pakiet',
        endDateTimeErr: 'Data i czas końcowy musi być później niż data i czas startowy'
      }
    }
  }

  translate(key: string): string {
    
    if(this.env.env === 'test'){this.language = "en_EN"};
    if (this.dictionary[this.language] != null) {
      return this.dictionary[this.language].values[key];
    }
  }
}
