import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslationService } from '../service/translation.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { PackageItem, PackageStatus, PackageType, PersonGenerate, Properties } from '../models/models';
import { HttpService } from '../service/http.service';
import { concatMap, switchMap, debounceTime } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import * as CryptoJS from 'crypto-js';
import { NgwWowService } from 'ngx-wow';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface PackageObj {
  name?: PackageType,
  price: number,
  packages?: PackageItem[]
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private router: Router,
    private translationService: TranslationService, private formBuilder: FormBuilder, private route: ActivatedRoute,
    private service: HttpService, private snackBar: MatSnackBar, private wowService: NgwWowService) {
    this.wowService.init();
  }

  initEnv = environment;
  env: Properties = null;
  registerForm: FormGroup;
  invalidCredentials = false;
  packageItems: PackageItem[] = [];
  packages: PackageObj[];
  currentPackage: PackageObj;
  emailPattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
  peselPattern = /^\d{11}$/;
  phonePattern = /^\d{9}$/;
  letterPattern = /^[A-Za-zĄąĆćĘęŁłŃńÓóŚśŹźŻż-]*$/;
  streetPattern = /^[A-Za-z ĄąĆćĘęŁłŃńÓóŚśŹźŻż.-]*$/;
  cityPattern = /^[A-Za-z ĄąĆćĘęŁłŃńÓóŚśŹźŻż-]*$/;
  postalCodePattern = /^\d{2}-\d{3}$/;
  numberPattern = /^[0-9]*$/;
  withoutSpecialCharacters = /^[a-zA-Z0-9!_.-]*$/
  serverErr = this.translationService.translate('serverError');
  userRegistered = this.translationService.translate('userRegistered');
  packageRemoved = this.translationService.translate('packageRemoved');
  packagesRemoved = this.translationService.translate('packagesRemoved');
  registerPending = false;
  customPackageLength: number = 0;
  packageProceed: boolean = false;
  packageStatus: PackageStatus = PackageStatus.FAILED;

  private validationMessages = {
    username: {
      required: this.translationService.translate('userRequired'),
      available: this.translationService.translate('available'),
      pattern: this.translationService.translate('allowedSigns') + 'a-zA-Z0-9!_.-'
    },
    name: {
      required: this.translationService.translate('nameRequired'),
      pattern: this.translationService.translate('nameIncorrect')
    },
    surname: {
      required: this.translationService.translate('surnameRequired'),
      pattern: this.translationService.translate('surnameIncorrect'),
    },
    password: {
      required: this.translationService.translate('passRequired'),
      minlength: this.translationService.translate('passMinLength'),
      pattern: this.translationService.translate('allowedSigns') + 'a-zA-Z0-9!_.-'
    },
    confirmPassword: {
      required: this.translationService.translate('confirmPassRequired'),
      notsame: this.translationService.translate('passNotMatch')
    },
    email: {
      required: this.translationService.translate('emailRequired'),
      pattern: this.translationService.translate('emailIncorrect'),
      available: this.translationService.translate('availableEmail')
    },
    package: {
      required: this.translationService.translate('packageRequired')
    },
    phone: {
      pattern: this.translationService.translate('phoneIncorrect')
    },
    city: {
      required: this.translationService.translate('cityRequired'),
      pattern: this.translationService.translate('cityIncorrect'),
    },
    postalCode: {
      required: this.translationService.translate('postalCodeRequired'),
      pattern: this.translationService.translate('postalCodeIncorrect') + ' XX-XXX'
    },
    street: {
      required: this.translationService.translate('streetRequired'),
      pattern: this.translationService.translate('streetIncorrect'),
    },
    houseNumber: {
      required: this.translationService.translate('houseNumberRequired'),
      pattern: this.translationService.translate('houseNumberIncorrect')
    },
    apartmentNumber: {
      pattern: this.translationService.translate('appartmentNumberIncorrect')
    },
    pesel: {
      required: this.translationService.translate('peselRequired'),
      pattern: this.translationService.translate('peselIncorrect')
    },
  };

  formErrors = {
    username: '',
    name: '',
    surname: '',
    password: '',
    confirmPassword: '',
    email: '',
    phone: '',
    package: '',
    city: '',
    postalCode: '',
    street: '',
    houseNumber: '',
    pesel: '',
    apartmentNumber: ''
  };

  httpCheckByUsername(service: HttpService) {
    return (control: FormControl) => {
      this.service.getPersonByUsername(control.value).pipe(switchMap(res => {

        if (res !== null) {
          control.setErrors({ available: 'dddd' });
          this.onControlValueChanged();
        }
        return of(res);
      })).subscribe(res => {

      }, error => {
        this.snackBar.open(this.serverErr, 'OK');
        console.log(error);
      });
    };
  }

  deletePackage(key: number) {
    if (key === -1) {
      this.customPackageLength = 0;
      this.packages[1].packages = [];
      this.packages[1].price = 0;
      this.snackBar.open(this.packagesRemoved, 'OK');
    } else {
      this.packages[1].price -= this.packages[1].packages[key].price;
      this.packages[1].packages.splice(key, 1);
      this.customPackageLength--;
      this.snackBar.open(this.packageRemoved, 'OK');
    }
  }

  addPackageRequest(param: PackageItem[]) {

    this.packageProceed = false;
    if (this.customPackageLength < this.packageItems.length) {

      try {
        param.forEach(p => {
          let ind = this.currentPackage.packages.find(f => f === p);

          if (typeof ind === 'undefined') {
            this.currentPackage.packages.push(p);
            this.packages[1].price += p.price;
            this.customPackageLength++;
            this.packageStatus = PackageStatus.ADDED;
          } else {
            this.packageStatus = PackageStatus.ALREADY_HAVE;
          }
        });
      } catch (e) {
        this.packageStatus = PackageStatus.FAILED;
      }
    } else {
      this.packageStatus = PackageStatus.FAILED;
    }
    this.packageProceed = true;
  }

  httpCheckByEmail(service: HttpService) {
    return (control: FormControl) => {
      this.service.getPersonByEmail(control.value).pipe(switchMap(res => {

        if (res !== null) {
          control.setErrors({ available: 'dddd' });
          this.onControlValueChanged();
        }

        return of(res);
      })).subscribe(res => {

      }, error => {
        this.snackBar.open(this.serverErr, 'OK');
        console.log(error);
      });
    };
  }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, this.httpCheckByUsername(this.service), Validators.pattern(this.withoutSpecialCharacters)]],
      name: ['', [Validators.required, Validators.pattern(this.letterPattern)]],
      surname: ['', [Validators.required, Validators.pattern(this.letterPattern)]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.pattern(this.withoutSpecialCharacters)]],
      confirmPassword: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(this.emailPattern), this.httpCheckByEmail(this.service)]],
      phone: ['', Validators.pattern(this.phonePattern)],
      package: ['', Validators.required],
      city: ['', [Validators.required, Validators.pattern(this.cityPattern)]],
      postalCode: ['', [Validators.required, Validators.pattern(this.postalCodePattern)]],
      street: ['', [Validators.required, Validators.pattern(this.streetPattern)]],
      houseNumber: ['', [Validators.required, Validators.pattern(this.numberPattern)]],
      apartmentNumber: ['', Validators.pattern(this.numberPattern)],
      pesel: ['', [Validators.required, Validators.pattern(this.peselPattern)]],
      terms: ['', [Validators.required]]
    }, { validators: [this.additionalCheck] });

    this.service.getPropertiesObservable().pipe(concatMap(props => {
      this.env = props;
      return this.service.getBasePackage();
    }
    ),
      concatMap(res => {

        let basePrice = 0;
        res.forEach(f => basePrice += f.price);
        this.packages = [];
        this.packages.push({ name: PackageType.BASIC, price: basePrice, packages: res });
        let cust: PackageItem[] = [];
        this.packages.push({ name: PackageType.OWN, price: 0, packages: cust });
        console.log();
        this.currentPackage = this.packages.filter(p => p.name === this.route.snapshot.params.package)[0];
        this.registerForm.controls.package.setValue(this.route.snapshot.params.package);
        this.registerForm.valueChanges.subscribe((value) => this.onControlValueChanged());
        this.onControlValueChanged();

        return this.service.getPackages();
      })).subscribe(val => {
        this.packageItems = val;
      }, err => {
        this.snackBar.open(this.serverErr, 'OK');
        console.log(err);
      });
  }

  additionalCheck(form: FormGroup) {

    const pass = form.controls.password.value;
    const confirmPass = form.controls.confirmPassword.value;
    if (pass === confirmPass) {
      let errors = form.controls.confirmPassword.errors;
      if (errors && errors['notsame'] !== null) {
        delete errors['notsame'];
      }
    } else {
      form.controls.confirmPassword.setErrors({ notsame: '' });
    }
  }

  onControlValueChanged() {
    const form = this.registerForm;
    this.currentPackage = this.packages.filter(p => p.name === this.registerForm.value.package)[0];
    // tslint:disable-next-line: forin
    for (const field in this.formErrors) {

      this.formErrors[field] = '';
      const control = form.get(field);


      if (control && control.dirty && !control.valid) {
        const validationMessages = this.validationMessages[field];
        // tslint:disable-next-line: forin
        for (const key in control.errors) {
          this.formErrors[field] += validationMessages[key] + ' ';
        }
      }
    }
  }

  clearFields() {
    // tslint:disable-next-line: forin
    for (const field in this.registerForm.controls) {
      this.registerForm.get(field).setValue('', { emitEvent: false });
    }
  }

  register() {

    this.registerPending = true;

    const param: PersonGenerate = ({
      username: this.registerForm.get('username').value,
      email: this.registerForm.get('email').value,
      name: this.registerForm.get('name').value,
      surname: this.registerForm.get('surname').value,
      phone: this.registerForm.get('phone').value,
      password: CryptoJS.AES.encrypt(this.registerForm.get('password').value, this.initEnv.secret).toString(),
      packageItemList: this.currentPackage.packages,
      city: this.registerForm.get('city').value,
      postalCode: this.registerForm.get('postalCode').value,
      street: this.registerForm.get('street').value,
      houseNumber: this.registerForm.get('houseNumber').value,
      apartmentNumber: this.registerForm.get('apartmentNumber').value,
      pesel: this.registerForm.get('pesel').value
    });

    this.service.register(param).subscribe(data => {
      this.router.navigateByUrl('');
      this.invalidCredentials = false;
      this.clearFields();
      this.registerPending = false;
      this.snackBar.open(this.userRegistered, 'OK');
    }, err => {
      this.registerPending = false;
      this.snackBar.open(this.serverErr, 'OK');
      this.invalidCredentials = true;
      this.clearFields();
    });
  }

}
