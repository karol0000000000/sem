import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import { TranslatePipe } from '../service/translate.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material';
import { NgwWowModule } from 'ngx-wow';
import { CustomMaterialModule } from '../core/material.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { RouterModule } from '@angular/router';
import { MockHttp } from '../mock/MockHttp';
import { TranslationService } from '../service/translation.service';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [RegisterComponent, TranslatePipe],
      imports: [ReactiveFormsModule, HttpClientModule, MatSnackBarModule, NgwWowModule, CustomMaterialModule, MDBBootstrapModule.forRoot(),
        RouterModule.forRoot([]), RouterModule],
      providers: [{ provide: HttpClient, useClass: MockHttp }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
