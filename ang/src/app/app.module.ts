import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, Inject, LOCALE_ID } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BasicHttpInterceptorService } from './service/basic-http-interceptor.service';
import { RegisterComponent } from './register/register.component';
import { TranslatePipe } from './service/translate.pipe';
import { CustomMaterialModule } from './core/material.module';
import { HomeComponent } from './home/home.component';
import { VisitsComponent } from './patient/visits/visits.component';
import { TestsComponent } from './patient/tests/tests.component';
import { MessengerComponent } from './messenger/messenger.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormatDatePipe } from './service/format-date.pipe';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { NgwWowModule } from 'ngx-wow';
import { HomePatientComponent } from './patient/home-patient/home-patient.component';
import { HomeAdminComponent } from './admin/home-admin/home-admin.component';
import { ErrorComponent } from './error/error.component';
import { VisitGenerateComponent } from './admin/visit-generate/visit-generate.component';
import { TestGenerateComponent } from './admin/test-generate/test-generate.component';
import { InjectableRxStompConfig, RxStompService, rxStompServiceFactory } from '@stomp/ng2-stompjs';
import { MessageWebSocketService } from './service/message-web-socket.service';
import { MAT_DATE_LOCALE } from '@angular/material';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { ClarityModule } from '@clr/angular';
import { InvoiceGenerateComponent } from './admin/invoice-generate/invoice-generate.component';
import { HomeMessagesComponent } from './admin/home-messages/home-messages.component';
import { HttpService } from './service/http.service';
import { TranslationService } from './service/translation.service';
import { VisitManageComponent } from './admin/visit-manage/visit-manage.component';
import { TestManageComponent } from './admin/test-manage/test-manage.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { RegisterPackageChoiceComponent } from './patient/register-package-choice/register-package-choice.component';
import { DoctorLicenseGenerateComponent } from './admin/doctor-license-generate/doctor-license-generate.component';
import { ProcedureManageComponent } from './admin/procedure-manage/procedure-manage.component';
import { ProcedureGenerateComponent } from './admin/procedure-generate/procedure-generate.component';
import { ProceduresComponent } from './patient/procedures/procedures.component';
import { Properties } from './models/models';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    RegisterComponent,
    TranslatePipe,
    HomeComponent,
    VisitsComponent,
    TestsComponent,
    MessengerComponent,
    FormatDatePipe,
    HomePatientComponent,
    HomeAdminComponent,
    ErrorComponent,
    VisitGenerateComponent,
    TestGenerateComponent,
    InvoiceGenerateComponent,
    HomeMessagesComponent,
    VisitManageComponent,
    TestManageComponent,
    RegisterPackageChoiceComponent,
    DoctorLicenseGenerateComponent,
    ProcedureManageComponent,
    ProcedureGenerateComponent,
    ProceduresComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MDBBootstrapModule.forRoot(),
    NgxMaterialTimepickerModule,
    NgwWowModule,
    ClarityModule,
    KeycloakAngularModule,
    InfiniteScrollModule
  ],
  exports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaterialTimepickerModule,
    NgwWowModule,
    ClarityModule,
    KeycloakAngularModule,
    InfiniteScrollModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: BasicHttpInterceptorService, multi: true
    },
    {
      provide: InjectableRxStompConfig
    },
    {
      provide: RxStompService,
      useFactory: rxStompServiceFactory,
      deps: [InjectableRxStompConfig]
    },
    MessageWebSocketService,
    { provide: MAT_DATE_LOCALE, useValue: 'pl-PL' },
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService, HttpService, TranslationService]
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function initializer(keycloak: KeycloakService, service: HttpService, 
  translationService: TranslationService): () => Promise<any> {
  return (): Promise<any> => {
    let env: Properties = null;
    return service.getPropertiesPromise().then(props => {
      env = props;
      return new Promise(async (resolve, reject) => {
        try {
          await keycloak.init({
            config: env['keycloakConfig'],
            initOptions: {
              checkLoginIframe: false
            },
            bearerExcludedUrls: ['/open']
          }).then(v => {
            if (sessionStorage.getItem('language')) {
              translationService.language = sessionStorage.getItem('language');
              resolve();
            } else {
              setCurrLanguage(translationService);
              resolve();
            }
          });
  
        } catch (error) {
          reject(error);
        }
      });
    });
  };
}

export function setCurrLanguage(translationService: TranslationService) {
  let language = navigator.language.replace('-', '_');
  if (language === 'pl_PL') {
    translationService.language = language;
    sessionStorage.setItem('language', language);
  } else {
    translationService.language = 'en_EN';
    sessionStorage.setItem('language', 'en_EN');
  }
}
