import { Component, OnInit } from '@angular/core';
import { KeycloakApiService } from '../service/keycloak/keycloak-api.service';
import { Roles } from '../models/models';
import { Router } from '@angular/router';
import { TranslationService } from '../service/translation.service';
import { HttpService } from '../service/http.service';
import { UtilsService } from '../service/utils.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  async ngOnInit() {
    await this.keycloakApi.isUserLoggedIn().then(val => this.logged = val);
  }

  language = this.translationService.language;
  languageFullName = '';
  visitsText = '';
  testsText = '';
  proceduresText = '';
  messagesText = '';
  messengerText = '';
  loginText = '';
  logoutText = '';

  constructor(private keycloakApi: KeycloakApiService, private router: Router,
    private translationService: TranslationService, private httpService: HttpService,
    private utils: UtilsService) {
    this.languageFullName = this.translationService.languages[this.translationService.language];
    this.loadTexts();
  }

  loadTexts() {
    this.visitsText = this.translationService.translate('visits');
    this.testsText = this.translationService.translate('tests');
    this.proceduresText = this.translationService.translate('procedures');
    this.messagesText = this.translationService.translate('messages');
    this.messengerText = this.translationService.translate('messenger');
    this.loginText = this.translationService.translate('login');
    this.logoutText = this.translationService.translate('logout');
  }

  onChange(value, e: any) {
    e.hide();
    this.translationService.language = value;
    this.languageFullName = this.translationService.languages[this.translationService.language];

    sessionStorage.setItem('language', value);
    this.utils.reloadLayout();
    this.loadTexts();
  }

  logged: boolean = false;

  closeNav(e: any) {
    e.hide();
  }

  login(nav: any) {
    this.closeNav(nav);
    this.keycloakApi.login();
  }

  logout(nav: any) {
    this.closeNav(nav);
    this.keycloakApi.logout();
  }

  isLoggedIn() {
    return this.logged;
  }

  isAdmin(): boolean {
    return this.keycloakApi.getRoles().find(role => role === Roles.ADMIN) ? true : false;
  }

  isPatient(): boolean {
    return this.keycloakApi.getRoles().find(role => role === Roles.PATIENT) ? true : false;
  }

}
