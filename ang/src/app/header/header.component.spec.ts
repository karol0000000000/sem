import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { CustomMaterialModule } from '../core/material.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { KeycloakAngularModule } from 'keycloak-angular';
import { Router } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MockHttp } from '../mock/MockHttp';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {

    const routerSpy = jasmine.createSpyObj('Router', ['navigate', 'navigateByUrl']);
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ],
      imports: [CustomMaterialModule, MDBBootstrapModule.forRoot(), KeycloakAngularModule, HttpClientModule],
      providers: [{provide: Router, useValue: routerSpy}, { provide: HttpClient, useClass: MockHttp }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
