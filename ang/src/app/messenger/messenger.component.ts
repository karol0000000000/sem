import { Component, OnInit, OnDestroy, AfterViewChecked, ViewChild, ElementRef } from '@angular/core';
import { TranslationService } from '../service/translation.service';
import { HttpService } from '../service/http.service';
import { NgwWowService } from 'ngx-wow';
import { MatSnackBar } from '@angular/material';
import { MessageSave, Message, Page } from '../models/models';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageWebSocketService } from '../service/message-web-socket.service';
import { KeycloakApiService } from '../service/keycloak/keycloak-api.service';
import { concatMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { UtilsService } from '../service/utils.service';

@Component({
  selector: 'app-messenger',
  templateUrl: './messenger.component.html',
  styleUrls: ['./messenger.component.css']
})
export class MessengerComponent implements OnDestroy, OnInit {

  @ViewChild('scrollMe', { static: false })
  private myScrollContainer: ElementRef;
  messages: Array<Message> = [];
  messagesLength = 0;
  messageForm: FormGroup;
  serverErr = this.translationService.translate('serverError');
  socketErr = this.translationService.translate('notSupportSocket');
  canSend: boolean = false;
  scrollDown: boolean = false;
  componentPending: boolean;
  loadingPending = false;

  page: Page = {
    pageNumber: 0,
    size: 5
  }

  constructor(private translationService: TranslationService, private httpService: HttpService, private wowService: NgwWowService,
    private snackBar: MatSnackBar, private formBuilder: FormBuilder, private router: Router, private messageSocket: MessageWebSocketService,
    private keycloakApi: KeycloakApiService, private utils: UtilsService) {
    this.wowService.init();
    this.componentPending = true;

    this.messageForm = this.formBuilder.group({
      message: [{ value: '', disabled: false }, Validators.required]
    });

    if (this.checkWebSocketSuport()) {
      this.messageForm.valueChanges.subscribe(val => this.onControlValueChanged());

      this.httpService.getMessagesLength().pipe(concatMap(num => {

        this.messagesLength = num;
        if (num < 5) this.page.size = num;
        if (num === 0) {
          return of(undefined);
        } else {
          return this.httpService.getAllMessagesByPage(this.page);
        }
      })).subscribe((res: Message[]) => {
        if (typeof res !== 'undefined') {
          this.messages = res;
          this.messageForm.controls['message'].enable();
          
          let id = setTimeout(() => {
            
            if(this.myScrollContainer.nativeElement){

              this.scrollToBottom();
              clearInterval(id);
            }
          }, 100);
        }
        this.componentPending = false;
      }, err => {
        console.log(err);
        this.componentPending = false;
        this.snackBar.open(this.serverErr, 'OK');
      });

    } else {
      this.messageForm.controls['message'].disable();
    }
  }

  onScrollUp() {
    this.httpService.getMessagesLength().pipe(concatMap(num => {

      let difference = num - this.messages.length;

      if (difference > 0) {
        this.loadingPending = true;
        
        if (difference <= 5) this.page.size = difference;
        else this.page.size = 5;

        return this.httpService.getMessagesAfterDate({ date: this.messages[0].date, page: this.page })
      } else {
        return of(undefined);
      }
    })).subscribe((val: Object) => {
      if (typeof val !== 'undefined') {
        try {
          this.myScrollContainer.nativeElement.scrollTop = 10;
          val['content'].forEach(v => this.messages.unshift(v));
        } catch (err) { }
      }
      this.loadingPending = false;
    })
  }

  ngOnInit(): void {
    this.messageSocket.connect();
    this.messageSocket.getSubject().subscribe(val => {
      this.messages.push(JSON.parse(val.body));
      if (this.scrollDown) {
        setTimeout(() => this.scrollToBottom(), 100);
      }
    })
  }

  ngOnDestroy(): void {
    this.messageSocket.disconnect();
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTo({ top: this.myScrollContainer.nativeElement.scrollHeight, behavior: 'smooth' });
    } catch (err) { }
  }

  scrollToTop(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTo({ top: 0, behavior: 'smooth' });
    } catch (err) { }
  }

  checkWebSocketSuport() {
    if (!window['WebSocket']) {
      this.snackBar.open(this.socketErr, 'OK');
      return false;
    } else {
      return true;
    }
  }

  formErrors = {
    message: ''
  };

  private validationMessages = {
    message: {
      required: this.translationService.translate('messageRequired')
    }
  };

  saveMessage() {

    if (this.canSend) {
      let tempMessage = this.messageForm.value['message'];
      this.messageForm.controls['message'].setValue('');
      let param: MessageSave = ({
        message: tempMessage,
        username: this.keycloakApi.getUsername(),
        dateTime: this.utils.getISOFromDateWithOffset(new Date())
      })

      this.messageSocket.send(param);

      this.formErrors.message = '';
    } else {
      this.messageForm.controls['message'].setValue('');
    }

  }

  onControlValueChanged() {

    var match = /[a-z]+/.exec(this.messageForm.value['message']);
    var match1 = /[\d]+/.exec(this.messageForm.value['message']);

    if ((match !== null || match1 !== null) && this.messageForm.value['message'].length > 0) {
      this.canSend = true;
    } else {
      this.canSend = false;
    }

    const form = this.messageForm;
    // tslint:disable-next-line: forin
    for (const field in this.formErrors) {

      this.formErrors[field] = '';
      const control = form.get(field);


      if (control && control.dirty && !control.valid) {
        const validationMessages = this.validationMessages[field];
        // tslint:disable-next-line: forin
        for (const key in control.errors) {
          this.formErrors[field] += validationMessages[key] + ' ';
        }
      }
    }
  }
}
