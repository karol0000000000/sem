import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessengerComponent } from './messenger.component';
import { TranslatePipe } from '../service/translate.pipe';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material';
import { NgwWowModule } from 'ngx-wow';
import { CustomMaterialModule } from '../core/material.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FormatDatePipe } from '../service/format-date.pipe';
import { Router } from '@angular/router';
import { MessageWebSocketService } from '../service/message-web-socket.service';
import { KeycloakAngularModule } from 'keycloak-angular';
import { Observable, of, Subject } from 'rxjs';
import { MockHttp } from '../mock/MockHttp';
import { MockMessageWebSocketService } from '../mock/MockMessageWebSocketService';
import { TranslationService } from '../service/translation.service';

describe('MessengerComponent', () => {
  let component: MessengerComponent;
  let fixture: ComponentFixture<MessengerComponent>;
  let compiled: HTMLElement;
  let translationService: TranslationService;

  beforeEach(async(() => {

    const routerSpy = jasmine.createSpyObj('Router', ['navigate', 'navigateByUrl']);

    TestBed.configureTestingModule({
      declarations: [ MessengerComponent, TranslatePipe, FormatDatePipe ],
      imports: [ReactiveFormsModule, HttpClientModule, MatSnackBarModule, NgwWowModule, CustomMaterialModule, InfiniteScrollModule, 
      FormsModule, KeycloakAngularModule ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{provide: MessageWebSocketService, useClass: MockMessageWebSocketService}, {provide: Router, useValue: routerSpy}, 
        { provide: HttpClient, useClass: MockHttp }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessengerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
    translationService = TestBed.get(TranslationService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should title contain messenger', () => {
  //   console.log(compiled.querySelector('.badgeCont'));
  //   expect(compiled.querySelector('mdb-badge span').textContent).toContain(translationService.translate('messenger'));
  // })
});
