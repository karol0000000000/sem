import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { KeycloakApiService } from './service/keycloak/keycloak-api.service';
import { Router } from '@angular/router';
import { Roles } from './models/models';
import { HttpService } from './service/http.service';
import { TranslationService } from './service/translation.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {

  onActivate(event) {
    window.scroll(0, 0);
  }

  constructor(private keycloakApi: KeycloakApiService, private router: Router, private httpService: HttpService,
    private translationService: TranslationService) {

    this.keycloakApi.isUserLoggedIn().then(val => {
      if (val) {
        let roles = this.keycloakApi.getRoles();
        if (roles.find(r => r === Roles.ADMIN)) {
          this.router.navigateByUrl('admin');
        } else if (roles.find(r => r === Roles.PATIENT)) {
          this.router.navigateByUrl('patient');
        }
      }
    });
  }

  title = 'Med';
}
