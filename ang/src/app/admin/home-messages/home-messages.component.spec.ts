import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeMessagesComponent } from './home-messages.component';
import { TranslatePipe } from 'src/app/service/translate.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material';
import { NgwWowModule } from 'ngx-wow';
import { CustomMaterialModule } from 'src/app/core/material.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormatDatePipe } from 'src/app/service/format-date.pipe';
import { MockHttp } from 'src/app/mock/MockHttp';

describe('HomeMessagesComponent', () => {
  let component: HomeMessagesComponent;
  let fixture: ComponentFixture<HomeMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeMessagesComponent, TranslatePipe, FormatDatePipe ],
      imports: [ReactiveFormsModule, HttpClientModule, MatSnackBarModule, NgwWowModule, CustomMaterialModule],
      providers: [{ provide: HttpClient, useClass: MockHttp }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
