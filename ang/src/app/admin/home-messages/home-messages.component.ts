import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { HttpService } from 'src/app/service/http.service';
import { MatSnackBar, MatPaginator } from '@angular/material';
import { TranslationService } from 'src/app/service/translation.service';
import { Page, HomeMessage } from 'src/app/models/models';
import { concatMap } from 'rxjs/operators';
import { NgwWowService } from 'ngx-wow';

@Component({
  selector: 'app-home-messages',
  templateUrl: './home-messages.component.html',
  styleUrls: ['./home-messages.component.css']
})
export class HomeMessagesComponent implements OnInit, AfterViewInit {

  ngAfterViewInit(): void {
    
    this.paginator._intl.itemsPerPageLabel = this.translationService.translate('itemsPerPageLabel');
    this.paginator._intl.nextPageLabel = this.translationService.translate('nextPageLabel');
    this.paginator._intl.previousPageLabel = this.translationService.translate('previousPageLabel');
    this.paginator._intl.firstPageLabel = this.translationService.translate('firstPageLabel');
    this.paginator._intl.lastPageLabel = this.translationService.translate('lastPageLabel');
  }

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;


  page: Page = {
    pageNumber: 0,
    size: 10
  }
  componentPending: boolean;
  serverErr = this.translationService.translate('serverError');
  dataLength: number = 0;
  pageData: HomeMessage[] = [];
  headElements: string[] = [this.translationService.translate('name'), this.translationService.translate('email'),
  this.translationService.translate('phone'), this.translationService.translate('company'),
  this.translationService.translate('message'), this.translationService.translate('date')]

  constructor(private httpService: HttpService, private snackBar: MatSnackBar, private translationService: TranslationService, private wowService: NgwWowService) {

    wowService.init();
    this.componentPending = true;
    this.httpService.getHomeMessageLength().pipe(concatMap(num => {
      this.dataLength = num;
      return this.httpService.getAllHomeMessageByPage(this.page)
    }))
      .subscribe(data => {
        this.pageData = data['content'];
        this.componentPending = false;
      }, err => {
        this.componentPending = false;
        this.snackBar.open(this.serverErr, 'OK');
        console.log(err);
      });
  }

  onPaginateChange(event) {

    this.componentPending = true;
    this.page.pageNumber = event.pageIndex;
    this.page.size = event.pageSize

    this.httpService.getAllHomeMessageByPage(this.page).subscribe(data => {
      this.pageData = data['content'];
      this.componentPending = false;
    }, err => {
      this.componentPending = false;
      this.snackBar.open(this.serverErr, 'OK');
      console.log(err);
    });
  }

  ngOnInit() {
  }

}
