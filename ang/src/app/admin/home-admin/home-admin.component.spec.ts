import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeAdminComponent } from './home-admin.component';
import { TranslatePipe } from 'src/app/service/translate.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material';
import { NgwWowModule } from 'ngx-wow';
import { CustomMaterialModule } from 'src/app/core/material.module';
import { CUSTOM_ELEMENTS_SCHEMA, Type } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { MockHttp } from 'src/app/mock/MockHttp';

describe('HomeAdminComponent', () => {
  let component: HomeAdminComponent;
  let fixture: ComponentFixture<HomeAdminComponent>;

  beforeEach(async(() => {

    const keycloakServiceSpy = jasmine.createSpyObj('KeycloakService', ['getUsername']);
    TestBed.configureTestingModule({
      declarations: [HomeAdminComponent, TranslatePipe],
      imports: [ReactiveFormsModule, HttpClientModule, MatSnackBarModule, NgwWowModule, CustomMaterialModule, 
        MDBBootstrapModule.forRoot(), KeycloakAngularModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{provide: KeycloakService, useValue: keycloakServiceSpy}, { provide: HttpClient, useClass: MockHttp }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
