import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/service/http.service';
import { MatSnackBar } from '@angular/material';
import { Person } from 'src/app/models/models';
import { TranslationService } from 'src/app/service/translation.service';
import { KeycloakApiService } from 'src/app/service/keycloak/keycloak-api.service';

@Component({
  selector: 'app-home-admin',
  templateUrl: './home-admin.component.html',
  styleUrls: ['./home-admin.component.css']
})
export class HomeAdminComponent implements OnInit {

  user: Person = { username: '' }
  serverErr = this.translationService.translate('serverError');

  constructor(private httpService: HttpService, private snackBar: MatSnackBar, private translationService: TranslationService, private keycloakApi: KeycloakApiService) {
    this.httpService.getPersonByUsername(this.keycloakApi.getUsername()).subscribe(res => {
      this.user = res;
    }, err => {
      this.snackBar.open(this.serverErr, 'OK');
      console.log(err);
    });
  }

  ngOnInit() {
    
  }

}
