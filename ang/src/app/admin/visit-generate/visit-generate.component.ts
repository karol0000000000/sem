import { Component, OnInit, Input } from '@angular/core';
import { NgwWowService } from 'ngx-wow';
import { FormGroup, FormBuilder, Validators, FormGroupDirective } from '@angular/forms';
import { HttpService } from 'src/app/service/http.service';
import { Office, License, LicenseType, LicenseFilter, Person, ServiceGenerate, ServiceType } from 'src/app/models/models';
import { concatMap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { TranslationService } from 'src/app/service/translation.service';
import { UtilsService } from 'src/app/service/utils.service';

@Component({
  selector: 'app-visit-generate',
  templateUrl: './visit-generate.component.html',
  styleUrls: ['./visit-generate.component.css']
})
export class VisitGenerateComponent implements OnInit {

  @Input()
  id;
  startTime: string = '';
  endTime: string = '';
  doctors: Person[];
  licenses: License[];
  offices: Office[];
  generateVisitForm: FormGroup;
  timeSpaceInMinutes: number[] = [];
  serverErr = this.translationService.translate('serverError');
  visitsGenerated = this.translationService.translate('visitsGenerated');
  noLicenseVisitDoctor = this.translationService.translate('noLicenseVisitDoctor');

  constructor(private wowService: NgwWowService, private formBuilder: FormBuilder, private httpService: HttpService,
    private snackBar: MatSnackBar, private translationService: TranslationService, private utils: UtilsService) {
    this.wowService.init();
    this.httpService.getAllDoctors().pipe(concatMap((doctors: Person[]) => {
      this.doctors = doctors;
      return this.httpService.getAllOffices();
    })).subscribe((offices: Office[]) => this.offices = offices, err => {
      console.log(err);
      this.snackBar.open(this.serverErr, 'OK');
    });

    for (let i = 5; i < 60; i++) {
      if (60 % i === 0 && i % 5 === 0) {
        this.timeSpaceInMinutes.push(i);
      }
    }
  }

  resetTime() {
    this.startTime = '12:00';
    this.endTime = '12:00';

    setTimeout(() => {
      this.startTime = '';
      this.endTime = '';
    }, 500);
  }

  close() {
    this.id.hide();
    this.generateVisitForm.reset();
    this.resetTime();
    this.utils.reloadLayout();
  }

  detectChangeStart(val) {
    this.startTime = val;
    this.generateVisitForm.updateValueAndValidity();
  }

  detectChangeEnd(val) {
    this.endTime = val;
    this.generateVisitForm.updateValueAndValidity();
  }

  checkIfAfter(group: FormGroup) {

    let startDate = group.controls.startDate.value ? new Date(group.controls.startDate.value) : undefined;
    let endDate = group.controls.endDate.value ? new Date(group.controls.endDate.value) : undefined;
    let control = group.controls.endDate;

    if (typeof startDate !== 'undefined' && typeof endDate !== 'undefined' && this.startTime && this.endTime) {

      let splitted: string[] = this.startTime.split(':');
      startDate.setHours(Number.parseInt(splitted[0]));
      startDate.setMinutes(Number.parseInt(splitted[1]));

      let splitted1: string[] = this.endTime.split(':');
      endDate.setHours(Number.parseInt(splitted1[0]));
      endDate.setMinutes(Number.parseInt(splitted1[1]));

      if (endDate.getTime() < startDate.getTime()) {
        control.setErrors({ wrongDate: true })
      } else {
        if (control.errors && control.errors['wrongDate']) {
          control.setErrors(null);
        }
      }
    }
  }

  generate(formDirective: FormGroupDirective) {
    let startTime = this.startTime.split(':');
    let endTime = this.endTime.split(':');

    let startDateTime = new Date(this.generateVisitForm.controls['startDate'].value);
    startDateTime.setHours(Number.parseInt(startTime[0]));
    startDateTime.setMinutes(Number.parseInt(startTime[1]));
    let endDateTime = new Date(this.generateVisitForm.controls['endDate'].value);
    endDateTime.setHours(Number.parseInt(endTime[0]));
    endDateTime.setMinutes(Number.parseInt(endTime[1]));

    let param: ServiceGenerate = {
      doctor: this.doctors[this.generateVisitForm.controls['doctor'].value],
      license: this.licenses[this.generateVisitForm.controls['license'].value],
      endDateTime: this.utils.getISOFromDateWithOffset(endDateTime),
      office: this.offices[this.generateVisitForm.controls['office'].value],
      startDateTime: this.utils.getISOFromDateWithOffset(startDateTime),
      timeSpaceInMinutes: this.generateVisitForm.controls['timeSpaceInMinutes'].value,
      type: ServiceType.VISIT
    }

    this.httpService.generateServices(param).subscribe(val => {
      this.snackBar.open(this.visitsGenerated, 'OK');
      this.generateVisitForm.reset();
      this.resetTime();
      formDirective.resetForm();
    }, err => {
      console.log(err);
      this.snackBar.open(this.serverErr, 'OK');
      this.generateVisitForm.reset();
      this.resetTime();
      formDirective.resetForm();
    });
  }

  onDoctorChoose() {

    this.generateVisitForm.controls['license'].setValue('');
    this.generateVisitForm.controls['license'].disable();

    let param: LicenseFilter = {
      doctor: this.doctors[this.generateVisitForm.value['doctor']],
      type: LicenseType.VISIT
    }

    this.httpService.getLicenseByDoctorAndType(param).subscribe(s => {
      if(s.length === 0){
        this.snackBar.open(this.noLicenseVisitDoctor, 'OK');
      }
      this.licenses = s;
      this.generateVisitForm.controls['license'].enable();
    }, err => {
      console.log(err);
      this.snackBar.open(this.serverErr, 'OK');
    });
  }

  ngOnInit() {
    this.generateVisitForm = this.formBuilder.group({
      doctor: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      timeSpaceInMinutes: ['', Validators.required],
      license: [{ value: '', disabled: true }, Validators.required],
      office: ['', Validators.required]
    }, {validators: this.checkIfAfter.bind(this)});
  }

}
