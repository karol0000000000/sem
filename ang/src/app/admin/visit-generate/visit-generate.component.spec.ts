import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitGenerateComponent } from './visit-generate.component';
import { TranslatePipe } from 'src/app/service/translate.pipe';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material';
import { NgwWowModule } from 'ngx-wow';
import { CustomMaterialModule } from 'src/app/core/material.module';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { MockHttp } from 'src/app/mock/MockHttp';

describe('VisitGenerateComponent', () => {
  let component: VisitGenerateComponent;
  let fixture: ComponentFixture<VisitGenerateComponent>;

  beforeEach(async(() => {

    const routerSpy = jasmine.createSpyObj('Router', ['navigate', 'navigateByUrl']);
    TestBed.configureTestingModule({
      declarations: [ VisitGenerateComponent, TranslatePipe ],
      imports: [ReactiveFormsModule, HttpClientModule, MatSnackBarModule, NgwWowModule, CustomMaterialModule, NgxMaterialTimepickerModule, 
      FormsModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{provide: Router, useValue: routerSpy}, { provide: HttpClient, useClass: MockHttp }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitGenerateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
