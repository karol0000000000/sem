import { Component, OnInit, Input } from '@angular/core';
import { Office, License, LicenseType, LicenseFilter, Person, ServiceGenerate, ServiceType } from 'src/app/models/models';
import { FormGroup, FormBuilder, Validators, FormGroupDirective } from '@angular/forms';
import { NgwWowService } from 'ngx-wow';
import { HttpService } from 'src/app/service/http.service';
import { MatSnackBar } from '@angular/material';
import { TranslationService } from 'src/app/service/translation.service';
import { concatMap } from 'rxjs/operators';
import { UtilsService } from 'src/app/service/utils.service';

@Component({
  selector: 'app-test-generate',
  templateUrl: './test-generate.component.html',
  styleUrls: ['./test-generate.component.css']
})
export class TestGenerateComponent implements OnInit {

  @Input()
  id;
  startTime: string = '';
  endTime: string = '';
  doctors: Person[];
  licenses: License[];
  offices: Office[];
  generateTestForm: FormGroup;
  timeSpaceInMinutes: number[] = [];
  serverErr = this.translationService.translate('serverError');
  testsGenerated = this.translationService.translate('testsGenerated');
  noLicenseTestDoctor = this.translationService.translate('noLicenseTestDoctor');

  constructor(private wowService: NgwWowService, private formBuilder: FormBuilder, private httpService: HttpService,
    private snackBar: MatSnackBar, private translationService: TranslationService, private utils: UtilsService) {
    this.wowService.init();
    this.httpService.getAllDoctors().pipe(concatMap((doctors: Person[]) => {
      this.doctors = doctors;
      return this.httpService.getAllOffices();
    })).subscribe((offices: Office[]) => this.offices = offices, err => {
      console.log(err);
      this.snackBar.open(this.serverErr, 'OK');
    });

    for (let i = 5; i < 60; i++) {
      if (60 % i === 0 && i % 5 === 0) {
        this.timeSpaceInMinutes.push(i);
      }
    }
  }

  detectChangeStart(val) {
    this.startTime = val;
    this.generateTestForm.updateValueAndValidity();
  }

  detectChangeEnd(val) {
    this.endTime = val;
    this.generateTestForm.updateValueAndValidity();
  }

  checkIfAfter(group: FormGroup) {

    let startDate = group.controls.startDate.value ? new Date(group.controls.startDate.value) : undefined;
    let endDate = group.controls.endDate.value ? new Date(group.controls.endDate.value) : undefined;
    let control = group.controls.endDate;

    if (typeof startDate !== 'undefined' && typeof endDate !== 'undefined' && this.startTime && this.endTime) {

      let splitted: string[] = this.startTime.split(':');
      startDate.setHours(Number.parseInt(splitted[0]));
      startDate.setMinutes(Number.parseInt(splitted[1]));

      let splitted1: string[] = this.endTime.split(':');
      endDate.setHours(Number.parseInt(splitted1[0]));
      endDate.setMinutes(Number.parseInt(splitted1[1]));

      if (endDate.getTime() < startDate.getTime()) {
        control.setErrors({ wrongDate: true })
      } else {
        if (control.errors && control.errors['wrongDate']) {
          control.setErrors(null);
        }
      }
    }
  }

  close() {
    this.id.hide();
    this.generateTestForm.reset();
    this.resetTime();
    this.utils.reloadLayout();
  }

  resetTime() {
    this.startTime = '12:00';
    this.endTime = '12:00';

    setTimeout(() => {
      this.startTime = '';
      this.endTime = '';
    }, 500);
  }

  generate(formDirective: FormGroupDirective) {
    let startTime = this.startTime.split(':');
    let endTime = this.endTime.split(':');

    let startDateTime = new Date(this.generateTestForm.controls['startDate'].value);
    startDateTime.setHours(Number.parseInt(startTime[0]));
    startDateTime.setMinutes(Number.parseInt(startTime[1]));
    let endDateTime = new Date(this.generateTestForm.controls['endDate'].value);
    endDateTime.setHours(Number.parseInt(endTime[0]));
    endDateTime.setMinutes(Number.parseInt(endTime[1]));

    let param: ServiceGenerate = {
      doctor: this.doctors[this.generateTestForm.controls['doctor'].value],
      license: this.licenses[this.generateTestForm.controls['license'].value],
      endDateTime: this.utils.getISOFromDateWithOffset(endDateTime),
      office: this.offices[this.generateTestForm.controls['office'].value],
      startDateTime: this.utils.getISOFromDateWithOffset(startDateTime),
      timeSpaceInMinutes: this.generateTestForm.controls['timeSpaceInMinutes'].value,
      type: ServiceType.TEST
    }

    this.httpService.generateServices(param).subscribe(val => {
      this.snackBar.open(this.testsGenerated, 'OK');
      this.generateTestForm.reset();
      this.resetTime();
      formDirective.resetForm();
    }, err => {
      console.log(err);
      this.snackBar.open(this.serverErr, 'OK');
      this.generateTestForm.reset();
      this.resetTime();
      formDirective.resetForm();
    });
  }

  onDoctorChoose() {

    this.generateTestForm.controls['license'].setValue('');
    this.generateTestForm.controls['license'].disable();

    let param: LicenseFilter = {
      doctor: this.doctors[this.generateTestForm.value['doctor']],
      type: LicenseType.TEST
    }

    this.httpService.getLicenseByDoctorAndType(param).subscribe(t => {
      if(t.length === 0){
        this.snackBar.open(this.noLicenseTestDoctor, 'OK');
      }
      this.licenses = t;
      this.generateTestForm.controls['license'].enable();
    }, err => {
      console.log(err);
      this.snackBar.open(this.serverErr, 'OK');
    });
  }

  ngOnInit() {
    this.generateTestForm = this.formBuilder.group({
      doctor: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      timeSpaceInMinutes: ['', Validators.required],
      license: [{ value: '', disabled: true }, Validators.required],
      office: ['', Validators.required]
    }, {validators: [this.checkIfAfter.bind(this)]});
  }

}

