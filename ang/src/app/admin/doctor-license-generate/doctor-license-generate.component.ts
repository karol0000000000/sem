import { Component, OnInit, Input } from '@angular/core';
import { NgwWowService } from 'ngx-wow';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { HttpService } from 'src/app/service/http.service';
import { MatSnackBar } from '@angular/material';
import { TranslationService } from 'src/app/service/translation.service';
import { License, DoctorLicenseGenerate, Person } from 'src/app/models/models';

@Component({
  selector: 'app-doctor-license-generate',
  templateUrl: './doctor-license-generate.component.html',
  styleUrls: ['./doctor-license-generate.component.css']
})
export class DoctorLicenseGenerateComponent implements OnInit {

  @Input()
  id;
  
  doctors: Person[];
  licenses: License[];

  generateDoctorLicenseForm: FormGroup;
  
  serverErr = this.translationService.translate('serverError');
  licenseDoctorGenerated = this.translationService.translate('doctorLicenseGenerated');
  noAvailable = this.translationService.translate('noLicenseForDoctor');

  constructor(private wowService: NgwWowService, private formBuilder: FormBuilder, private httpService: HttpService,
    private snackBar: MatSnackBar, private translationService: TranslationService) {
    this.wowService.init();
    this.httpService.getAllDoctors()
    .subscribe((doctors: Person[]) => this.doctors = doctors, err => {
      console.log(err);
      this.snackBar.open(this.serverErr, 'OK');
    });
  }

  close() {
    this.id.hide();
    this.generateDoctorLicenseForm.reset();
  }

  generate(formDirective: FormGroupDirective) {

    let param: DoctorLicenseGenerate = {
      doctor: this.doctors[this.generateDoctorLicenseForm.controls['doctor'].value],
      license: this.licenses[this.generateDoctorLicenseForm.controls['license'].value]
    }

    this.httpService.generateDoctorLicense(param).subscribe(val => {
      this.snackBar.open(this.licenseDoctorGenerated, 'OK');
      this.generateDoctorLicenseForm.reset();
      formDirective.resetForm();
    }, err => {
      console.log(err);
      this.snackBar.open(this.serverErr, 'OK');
      this.generateDoctorLicenseForm.reset();
      formDirective.resetForm();
    });
  }

  onDoctorChoose() {

    this.generateDoctorLicenseForm.controls['license'].setValue('');
    this.generateDoctorLicenseForm.controls['license'].disable();

    this.httpService.getPossibleLicensesForDoctor(this.doctors[this.generateDoctorLicenseForm.value['doctor']]).subscribe(s => {
      this.licenses = s;
      if(this.licenseDoctorGenerated.length === 0){
        this.snackBar.open(this.noAvailable, 'OK');
      }
      this.generateDoctorLicenseForm.controls['license'].enable();
    }, err => {
      console.log(err);
      this.snackBar.open(this.serverErr, 'OK');
    });
  }

  ngOnInit() {
    this.generateDoctorLicenseForm = this.formBuilder.group({
      doctor: ['', Validators.required],
      license: [{ value: '', disabled: true }, Validators.required]
    });
  }

}
