import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorLicenseGenerateComponent } from './doctor-license-generate.component';
import { TranslatePipe } from 'src/app/service/translate.pipe';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material';
import { NgwWowModule, NgwWowService } from 'ngx-wow';
import { CustomMaterialModule } from 'src/app/core/material.module';
import { MockHttp } from 'src/app/mock/MockHttp';

describe('DoctorLicenseGenerateComponent', () => {
  let component: DoctorLicenseGenerateComponent;
  let fixture: ComponentFixture<DoctorLicenseGenerateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorLicenseGenerateComponent, TranslatePipe ],
      imports: [ReactiveFormsModule, HttpClientModule, MatSnackBarModule, NgwWowModule, CustomMaterialModule],
      providers: [{ provide: HttpClient, useClass: MockHttp }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorLicenseGenerateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
