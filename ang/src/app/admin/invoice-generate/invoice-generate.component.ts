import { Component, OnInit, Input } from '@angular/core';
import { NgwWowService } from 'ngx-wow';
import { FormGroup, FormBuilder, Validators, FormGroupDirective } from '@angular/forms';
import { HttpService } from 'src/app/service/http.service';
import { Person, InvoiceGenerate } from 'src/app/models/models';
import { MatSnackBar } from '@angular/material';
import { TranslationService } from 'src/app/service/translation.service';
import { UtilsService } from 'src/app/service/utils.service';

@Component({
  selector: 'app-invoice-generate',
  templateUrl: './invoice-generate.component.html',
  styleUrls: ['./invoice-generate.component.css']
})
export class InvoiceGenerateComponent implements OnInit {

  @Input()
  id;
  users: Person[];
  generateInvoiceForm: FormGroup;
  serverErr = this.translationService.translate('serverError');
  invoiceGenerated = this.translationService.translate('newInvoiceGenerated');
  numberPattern = /^[0-9]*$/;

  constructor(private wowService: NgwWowService, private formBuilder: FormBuilder, private httpService: HttpService,
    private snackBar: MatSnackBar, private translationService: TranslationService, private utils: UtilsService) {
    this.wowService.init();
    this.httpService.getAllPatients().subscribe((u: Person[]) => this.users = u.filter(i => i.username !== 'admin'));
  }

  close() {
    this.id.hide();
    this.generateInvoiceForm.reset();
  }

  generate(formDirective: FormGroupDirective) {
  
    let param: InvoiceGenerate = {
      patient: this.users[this.generateInvoiceForm.controls['user'].value],
      exposure: this.utils.getISOFromDateWithOffset(new Date(this.generateInvoiceForm.controls['exposure'].value)),
      deadline: this.utils.getISOFromDateWithOffset(new Date(this.generateInvoiceForm.controls['deadline'].value)),
      fee: this.generateInvoiceForm.controls['fee'].value,
      paid: false
    }

    this.httpService.generateInvoice(param).subscribe(val => {
      this.snackBar.open(this.invoiceGenerated, 'OK');
      this.generateInvoiceForm.reset();
      formDirective.resetForm();
    }, err => {
      console.log(err);
      this.snackBar.open(this.serverErr, 'OK');
      this.generateInvoiceForm.reset();
      formDirective.resetForm();
    });
  }

  ngOnInit() {
    this.generateInvoiceForm = this.formBuilder.group({
      user: ['', Validators.required],
      exposure: ['', Validators.required],
      deadline: ['', Validators.required],
      fee: ['', [Validators.required, Validators.pattern(this.numberPattern)]]
    });
  }

}
