import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcedureManageComponent } from './procedure-manage.component';
import { TranslatePipe } from 'src/app/service/translate.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material';
import { NgwWowModule } from 'ngx-wow';
import { CustomMaterialModule } from 'src/app/core/material.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormatDatePipe } from 'src/app/service/format-date.pipe';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MockHttp } from 'src/app/mock/MockHttp';

describe('ProcedureManageComponent', () => {
  let component: ProcedureManageComponent;
  let fixture: ComponentFixture<ProcedureManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureManageComponent, TranslatePipe, FormatDatePipe ],
      imports: [ReactiveFormsModule, HttpClientModule, MatSnackBarModule, NgwWowModule, CustomMaterialModule, MDBBootstrapModule.forRoot()],
      providers: [{ provide: HttpClient, useClass: MockHttp }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
