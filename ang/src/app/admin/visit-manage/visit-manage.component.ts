import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { NgwWowService } from 'ngx-wow';
import { HttpService } from 'src/app/service/http.service';
import { MatSnackBar, MatPaginator } from '@angular/material';
import { TranslationService } from 'src/app/service/translation.service';
import { Page, Person, ServiceType, ServicePage, Service } from 'src/app/models/models';
import { concatMap, map } from 'rxjs/operators';
import { FormBuilder, FormArray, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-visit-manage',
  templateUrl: './visit-manage.component.html',
  styleUrls: ['./visit-manage.component.css']
})
export class VisitManageComponent implements AfterViewInit {

  ngAfterViewInit(): void {

    this.paginator._intl.itemsPerPageLabel = this.translationService.translate('itemsPerPageLabel');
    this.paginator._intl.nextPageLabel = this.translationService.translate('nextPageLabel');
    this.paginator._intl.previousPageLabel = this.translationService.translate('previousPageLabel');
    this.paginator._intl.firstPageLabel = this.translationService.translate('firstPageLabel');
    this.paginator._intl.lastPageLabel = this.translationService.translate('lastPageLabel');
  }

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(private wowService: NgwWowService, private httpService: HttpService, private snackBar: MatSnackBar,
    private translationService: TranslationService, private fb: FormBuilder) {
    this.wowService.init();
    this.componentPending = true;

    this.httpService.getServicesLength({type: ServiceType.VISIT}).pipe(concatMap(num => {
      this.dataLength = num;
      return this.httpService.getAllServicesByPage(this.page)
    }), concatMap(visits => {
      this.prepareData(visits);
      return this.httpService.getAllPatients();
    }))
      .subscribe(users => {
        this.users = users.filter(u => u.username !== 'admin');
        let i = this.noUser.split(' ');
        this.users.unshift({ id: -1, name: i[0], surname: i[1] });
        this.filteredUsers = this.users;
        this.componentPending = false;
      }, err => {
        this.componentPending = false;
        this.snackBar.open(this.serverErr, 'OK');
        console.log(err);
      });
  }

  userGroup: FormGroup = this.fb.group({ userForm: this.fb.array([]), currSelectState: false });
  userForm = <FormArray>this.userGroup.controls.userForm;
  componentPending: boolean;

  page: ServicePage = {
    pageNumber: 0,
    size: 10,
    type: ServiceType.VISIT
  }

  noUser = this.translationService.translate('noUser');
  users = [];
  filteredUsers = [];

  serverErr = this.translationService.translate('serverError');
  userErr = this.translationService.translate('incorrectUser');
  visitRemoved = this.translationService.translate('visitRemoved');
  visitsRemoved = this.translationService.translate('visitsRemoved');
  visitChanged = this.translationService.translate('visitChanged');
  noChanges = this.translationService.translate('noChanges');
  dataLength: number = 0;
  temporaryUserValue = '';

  pageData: Service[] = [];
  headElements: string[] = [this.translationService.translate('doctor'), this.translationService.translate('type'),
  this.translationService.translate('office'), this.translationService.translate('user'),
  this.translationService.translate('time'), this.translationService.translate('delete')];

  onPaginateChange(event) {

    this.componentPending = true;
    this.page.pageNumber = event.pageIndex;
    this.page.size = event.pageSize

    this.httpService.getAllServicesByPage(this.page).subscribe(data => {
      this.prepareData(data);
    }, err => {
      this.componentPending = false;
      this.snackBar.open(this.serverErr, 'OK');
      console.log(err);
    });
  }

  prepareData(data) {
    this.pageData = data;
    this.userForm.controls = [];
    this.pageData.forEach(x => this.userForm.push(this.fb.group({ data: x.patient ? x.patient.name + ' ' + x.patient.surname : this.noUser, select: false })));
    this.userForm.controls.forEach((control: FormGroup) => {
      control.controls['data'].disable();
      this.componentPending = false;
      control.controls['data'].valueChanges.pipe(map((field: string) => {
        return field ? this.filterStates(field) : this.users.slice();
      })).subscribe(val => {
        this.filteredUsers = val;
      }, err => {
        this.snackBar.open(this.serverErr, 'OK');
        console.log(err);
      });
    });
    this.userGroup.get('currSelectState').setValue(false);
  }

  onRemove(index: number[]) {

    let param: number[] = [];
    index.forEach(i => {
      param.push(this.pageData[i].id);
    })

    this.httpService.deleteService({ ids: param }).pipe(concatMap(() => {

      if (!(this.dataLength - index.length > (this.page.pageNumber * this.page.size)) && this.page.pageNumber > 0) {
        this.page.pageNumber = this.page.pageNumber - 1;
      }
      return this.httpService.getServicesLength({type: ServiceType.VISIT});
    }), concatMap(length => {
      this.dataLength = length;
      return this.httpService.getAllServicesByPage(this.page);
    })).subscribe(data => {
      this.prepareData(data);
      this.snackBar.open(index.length === 1 ? this.visitRemoved : this.visitsRemoved, 'OK');
    }, err => {
      this.snackBar.open(this.serverErr, 'OK');
      console.log(err);
    })
  }

  onRemoveAll() {
    let indexes: number[] = [];
    this.userForm.controls.forEach((control: FormGroup, j) => {
      if (control.get('select').value) {
        indexes.push(j);
      }
    });
    if (indexes.length > 0) {
      this.onRemove(indexes)
    } else {
      this.snackBar.open(this.noChanges, 'OK');
    }
  }

  onBuild(index) {
    this.temporaryUserValue = this.userForm.controls[index]['controls']['data'].value;
    this.userForm.controls[index]['controls']['data'].enable();
    this.userForm.controls[index]['controls']['data'].setValue('');
  }

  onDone(index) {
    let temporarySplitted = this.temporaryUserValue.split(' ');
    this.userForm.controls[index]['controls']['data'].disable();
    let currValue = this.userForm.controls[index]['controls']['data'].value;
    let currValueTab = currValue.split(' ');
    let foundUser = this.users.find(x => x.name === currValueTab[0] && x.surname === currValueTab[1]);
    if (typeof foundUser === 'undefined') {
      this.userForm.controls[index]['controls']['data'].setValue(this.temporaryUserValue);
      this.snackBar.open(this.userErr, 'OK');
    } else if (temporarySplitted[0] === foundUser.name && temporarySplitted[1] === foundUser.surname) {
      this.snackBar.open(this.noChanges, 'OK');
    } else {
      let currVisit = this.pageData[index];
      currVisit.patient = foundUser.id === -1 ? null : foundUser;
      this.httpService.setUserForService(currVisit).subscribe(res => {
        this.snackBar.open(this.visitChanged, 'OK');
      }, err => {
        this.snackBar.open(this.serverErr, 'OK');
        console.log(err);
      })
    }
  }

  onClear(index) {
    this.userForm.controls[index]['controls']['data'].disable();
    this.userForm.controls[index]['controls']['data'].setValue(this.temporaryUserValue);
  }

  filterStates(value: string): Person[] {
    return this.users.filter(user => (user.name + ' ' + user.surname).toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  selectAll() {
    this.userForm.controls.forEach((control: FormGroup) => {
      control.controls['select'].setValue(!this.userGroup.get('currSelectState').value);
    });
  }
}
