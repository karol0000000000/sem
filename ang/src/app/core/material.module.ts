import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatCardModule, MatDialogModule, MatInputModule, MatTableModule, MatSnackBarModule,
  MatToolbarModule, MatMenuModule, MatIconModule, MatProgressSpinnerModule, MatFormFieldModule, MatSidenavModule, 
  MatListModule, MatChipsModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule, MAT_SNACK_BAR_DEFAULT_OPTIONS, 
  MatCheckboxModule, MatPaginatorModule, MatAutocompleteModule
} from '@angular/material';
@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatDialogModule,
    MatTableModule,
    MatMenuModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatListModule,
    MatChipsModule,
    MatSelectModule,
    MatDatepickerModule, 
    MatNativeDateModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatAutocompleteModule
  ],
  exports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatDialogModule,
    MatTableModule,
    MatMenuModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatListModule,
    MatChipsModule,
    MatSelectModule, 
    MatDatepickerModule, 
    MatNativeDateModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatAutocompleteModule
  ],
  providers: [
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 5000}}
  ]
})
export class CustomMaterialModule { }