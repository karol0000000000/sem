import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { TranslatePipe } from '../service/translate.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { CustomMaterialModule } from '../core/material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MockHttp } from '../mock/MockHttp';
import { Router } from '@angular/router';
import { HttpService } from '../service/http.service';
import { License, LicenseType, PackageItem } from '../models/models';
import { of } from 'rxjs';

const exampleLicense: License = {
  name: 'test',
  type: LicenseType.TEST
}
const examplePackage: PackageItem[] = [{
  id: 1,
  license: exampleLicense,
  name: 'test',
  price: 100
}]

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let domEl: HTMLElement;
  let router: Router;
  let service: HttpService;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [ HomeComponent, TranslatePipe ],
      imports: [ReactiveFormsModule, CustomMaterialModule, HttpClientModule, RouterTestingModule.withRoutes([])],
      providers: [{ provide: HttpClient, useClass: MockHttp }]
    })
    .compileComponents();

    service = TestBed.get(HttpService);
    spyOn(service, 'getBasePackage').and.returnValue(of(examplePackage));
    // spyOn(service, 'getBasePackage').and.callFake(() => {
    //   return of(examplePackage);
    // })
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    fixture.detectChanges();
    domEl = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have welcome text string', () => {
    expect(domEl.querySelector('#home h1').textContent).toContain('Welcome');
  });

  it('should redirect to registaration page', () => {

    const navigateSpy = spyOn(router, 'navigateByUrl');
    const button: HTMLButtonElement = domEl.querySelector('#buyNow');
    button.click();
    expect(button.textContent).toContain('Take advantage of the offer now');
    expect(navigateSpy).toHaveBeenCalledWith('register/BASIC');
    expect(service.getBasePackage).toHaveBeenCalledTimes(1);
  });

  it('should mat-error has not and next has error message', () => {
    let matError: HTMLElement = domEl.querySelector('#mail mat-error');
    const emailInput: HTMLInputElement = domEl.querySelector('#mail input');
    const phoneInput: HTMLInputElement = domEl.querySelector('#phone input');
    expect(matError).toBeNull();

    setInput("test", emailInput);

    // matError = domEl.querySelector('#mail mat-error');
    // expect(matError).toBeTruthy();
    expect(emailInput.value).toContain('test');
    expect(component.formErrors['email']).toContain('Email is incorrect');
    // expect(matError.textContent).toContain('Email is incorrect');
  });

  function setInput(text: string, input: HTMLInputElement) {
    input.value = text;
    input.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    return fixture.whenStable();
  }

});
