import { Component } from '@angular/core';
import { NgwWowService } from 'ngx-wow';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslationService } from '../service/translation.service';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { HttpService } from '../service/http.service';
import { HomeMessage, PackageItem} from '../models/models';
import { UtilsService } from '../service/utils.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  constructor(private router: Router,
    private translationService: TranslationService, private formBuilder: FormBuilder,
    private service: HttpService, private snackBar: MatSnackBar, private wowService: NgwWowService, private utils: UtilsService) {

      this.service.getBasePackage().subscribe(val => {
        this.basicPackage = val;
        val.forEach(i => {
          this.basicPackagePrice += i.price; 
        })
      }, err => {
        this.snackBar.open(this.serverErr, 'OK');
        console.log(err);
      })
  }

  ownPackageLabels: string[] = [this.translationService.translate('ownLabel1'), 
  this.translationService.translate('ownLabel2'), this.translationService.translate('ownLabel3')];

  basicPackagePrice: number = 0;
  basicPackage: PackageItem[];
  messageForm: FormGroup;
  invalidCredentials = false;
  emailPattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
  phonePattern = /^\d{9}$/;
  letterPattern = /^[A-Za-zĄąĆćĘęŁłŃńÓóŚśŹźŻż]*$/;
  withoutSpecialCharacters = /^[a-zA-Z0-9!_.-]*$/
  serverErr = this.translationService.translate('serverError');
  messageSent = this.translationService.translate('homeMessageSent');

  private validationMessages = {
    name: {
      required: this.translationService.translate('nameRequired'),
      pattern: this.translationService.translate('nameIncorrect')
    },
    email: {
      required: this.translationService.translate('emailRequired'),
      pattern: this.translationService.translate('emailIncorrect')
    },
    message: {
      required: this.translationService.translate('messageRequired')
    },
    phone: {
      pattern: this.translationService.translate('phoneIncorrect'),
    },
    company: {
      pattern: this.translationService.translate('allowedSigns') + 'a-zA-Z0-9!_.-'
    }
  };

  formErrors = {
    name: '',
    email: '',
    message: '',
    phone: '',
    company: ''
  };

  ngOnInit() {

    this.messageForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern(this.letterPattern)]],
      email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      message: ['', Validators.required],
      phone: ['', Validators.pattern(this.phonePattern)],
      company: ['', Validators.pattern(this.withoutSpecialCharacters)]
    });

    this.messageForm.valueChanges.subscribe((value) => this.onControlValueChanged());
    this.onControlValueChanged();

    this.wowService.init();
  }

  onControlValueChanged() {

    const form = this.messageForm;
    // tslint:disable-next-line: forin
    for (const field in this.formErrors) {

      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const validationMessages = this.validationMessages[field];
        // tslint:disable-next-line: forin
        for (const key in control.errors) {
          this.formErrors[field] += validationMessages[key] + ' ';
        }
      }
    }
  }

  clearFields() {
    // tslint:disable-next-line: forin
    for (const field in this.messageForm.controls) {
      this.messageForm.get(field).setValue('', { emitEvent: false });
    }
  }

  send(formDirective: FormGroupDirective) {

    let param: HomeMessage = {
      name: this.messageForm.get('name').value,
      email: this.messageForm.get('email').value,
      company: this.messageForm.get('company').value,
      message: this.messageForm.get('message').value,
      phone: this.messageForm.get('phone').value,
      dateTime: this.utils.getISOFromDateWithOffset(new Date())
    }

    this.service.saveHomeMessage(param).subscribe(data => {
      this.invalidCredentials = false;
      this.clearFields();
      formDirective.resetForm();
      this.snackBar.open(this.messageSent, 'OK');
    }, err => {
      this.snackBar.open(this.serverErr, 'OK');
      this.invalidCredentials = true;
      this.clearFields();
      formDirective.resetForm();
    });
  }

  onBuyClick(plan: string) {
    this.router.navigateByUrl('register/' + plan);
  }

}
