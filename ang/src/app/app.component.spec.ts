import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule, Router } from '@angular/router';
import { CustomMaterialModule } from './core/material.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { KeycloakAngularModule } from 'keycloak-angular';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MockHttp } from './mock/MockHttp';

describe('AppComponent', () => {

  const routerSpy = jasmine.createSpyObj('Router', ['navigate', 'navigateByUrl']);
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, HeaderComponent, FooterComponent
      ], imports: [RouterModule, RouterModule.forRoot([]), CustomMaterialModule, MDBBootstrapModule.forRoot(), KeycloakAngularModule, HttpClientModule],
      providers: [{provide: Router, useValue: routerSpy}, { provide: HttpClient, useClass: MockHttp }]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Med'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Med');
  });

  // it('should render title in a h1 tag', () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to ang!');
  // });
});
