
export interface PropertiesDTO {
    env: string;
    production: boolean;
    address_frontend: string;
    port_frontend: string;
    address_websocket_backend: string;
    address_backend: string;
    port_backend: string;
    keycloak_url: string;
    keycloak_realm: string;
    keycloak_clientId: string;
    keycloak_secret: string;
}

export interface Properties {
    env: string,
    production: boolean;
    address_frontend: string,
    port_frontend: string,
    address_websocket_backend: string,
    address_backend: string,
    port_backend: string,
    keycloakConfig: {
        url: string,
        realm: string,
        clientId: string,
        credentials: {
            secret: string
        }
    }
}

export const enum Roles {
    ADMIN = 'admin',
    PATIENT = 'patient'
}

export const enum PackageStatus {
    ADDED = "ADDED",
    ALREADY_HAVE = "ALREADY_HAVE",
    FAILED = "FAILED"
}

export const enum PackageType {
    BASIC = "BASIC",
    OWN = "OWN"
}

export interface Office {
    id?: number;
    number: number;
}

export interface ServiceTypePayload{
    type: ServiceType
}

export interface PackageItem {
    id?: number;
    name: string;
    price: number;
    license: License
}

export interface PersonGenerate {
    username: string,
    password: string,
    name: string,
    surname: string,
    email: string,
    phone: string,
    packageItemList: PackageItem[],
    city?: string,
    postalCode?: string,
    street?: string,
    houseNumber?: number,
    apartmentNumber?: number,
    pesel?: number;
}

export interface ServiceAvailability {
    doctorId: number,
    licenseId: number
}

export interface PackageItemPatient {
    id?: number,
    item: PackageItem
    patient: Person
}

export interface PackageItemPersonFilter {
    item?: PackageItem
    username?: string,
    type?: LicenseType
}

export interface License {
    id?: number;
    type: string;
    name: string;
}

export interface DoctorLicense {
    id: number;
    doctor: Person;
    license: License;
}

export interface Person {
    id?: number;
    username: string;
    password?: string;
    name?: string;
    surname?: string;
    email?: string;
    phone?: string;
    type?: string;
    city?: string;
    postalCode?: string;
    street?: string;
    houseNumber?: number;
    apartmentNumber?: number;
    pesel?: number;
}

export interface Service {
    id?: number;
    doctor: Person;
    time: Date;
    license: License;
    office: Office;
    patient: Person;
}

export interface ServiceBook {
    doctorName?: string;
    doctorSurname?: string;
    // 2019-08-03T23:04:16.978Z
    dateTime: Date;
    licenseName?: string;
    username: string;
    wantToBook: boolean;
    type: ServiceType
}

export interface ServiceFilter {

    doctorName?: string;
    doctorSurname?: string;
    // yyyy.MM.dd
    date?: string;
    licenseName?: string;
    username?: string;
    page?: Page,
    type: ServiceType
}

export interface Invoice {
    invoiceId?: number;
    patient: Person;
    exposure: Date;
    deadline: Date;
    fee: number;
    paid: boolean;
}

export interface InvoiceGenerate {
    exposure: string,
    deadline: string,
    fee: number,
    patient: Person,
    paid: boolean
}

export interface MessagesAfterDate {
    date: Date,
    page: Page
}

export interface LicenseFilter {
    type?: string;
    name?: string;
    doctor?: Person
    item?: PackageItem,
    username?: string
}

export interface PackageItemFilter {
    name: string;
}

export interface PersonFilter {
    username: string;
}

export interface Message {
    id: number,
    message: string,
    person: Person,
    date: Date
}

export interface MessageSave {
    message: string,
    username: string
    dateTime: string
}

export interface ServiceGenerate {
    doctor: Person;
    startDateTime: string;
    endDateTime: string;
    timeSpaceInMinutes: number;
    license: License;
    office: Office;
    type: ServiceType;
}

export enum LicenseType {
    TEST = 'TEST',
    VISIT = 'VISIT',
    PROCEDURE = 'PROCEDURE'
}

export enum ServiceType {
    TEST = 'TEST',
    VISIT = 'VISIT',
    PROCEDURE = 'PROCEDURE'
}

export enum PersonType {
    PATIENT = 'PATIENT',
    DOCTOR = 'DOCTOR',
    ADMIN = 'ADMIN'
}

export interface DoctorLicenseFilter {
    name?: string,
    doctor?: Person;
    type?: LicenseType
}

export interface DoctorLicenseGenerate {
    doctor: Person,
    license: License
}

export interface LicenseGenerate {
    type: string;
    name: string;
}

export interface HomeMessage {
    homeMessageId?: number,
    name: string,
    email: string,
    phone: string,
    company: string,
    message: string,
    dateTime: string
}

export interface Page {
    pageNumber: number,
    size: number
}

export interface ServicePage {
    pageNumber: number,
    size: number,
    type: ServiceType
}

export interface idsToDelete {
    ids: Array<number>;
}
