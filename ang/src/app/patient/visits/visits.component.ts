import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { HttpService } from 'src/app/service/http.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslationService } from 'src/app/service/translation.service';
import { Page, License, LicenseType, DoctorLicenseFilter, LicenseFilter, Person, ServicePage, ServiceType, Service, ServiceFilter, ServiceBook, ServiceAvailability } from 'src/app/models/models';
import { MatSnackBar, MatPaginator } from '@angular/material';
import { NgwWowService } from 'ngx-wow';
import { concatMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { KeycloakApiService } from 'src/app/service/keycloak/keycloak-api.service';

@Component({
  selector: 'app-visits',
  templateUrl: './visits.component.html',
  styleUrls: ['./visits.component.css']
})
export class VisitsComponent implements OnInit, AfterViewInit {

  ngAfterViewInit(): void {

    this.paginator._intl.itemsPerPageLabel = this.translationService.translate('itemsPerPageLabel');
    this.paginator._intl.nextPageLabel = this.translationService.translate('nextPageLabel');
    this.paginator._intl.previousPageLabel = this.translationService.translate('previousPageLabel');
    this.paginator._intl.firstPageLabel = this.translationService.translate('firstPageLabel');
    this.paginator._intl.lastPageLabel = this.translationService.translate('lastPageLabel');
  }

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  page: ServicePage = {
    pageNumber: 0,
    size: 10,
    type: ServiceType.VISIT
  }

  yourVisitsLength: number = 0;
  yourVisits: Service[] = [];
  filteredVisits: Service[] = [];
  visitForm: FormGroup;
  componentPending = true;

  headers: string[] = [this.translationService.translate('doctor'),
  this.translationService.translate('type'),
  this.translationService.translate('time'),
  this.translationService.translate('office'),
  this.translationService.translate('cancel')];

  filteredHeaders: string[] = [];
  licenses: License[] = [];
  doctors: Person[] = [];
  showAddForm = false;
  showFiltered = false;
  wantBook = false;
  valid: boolean = false;
  serverErr = this.translationService.translate('serverError');
  noDoctorLicense = this.translationService.translate('noDoctorLicense');
  bookVisitNotify = this.translationService.translate('bookVisit');
  cancelVisitNotify = this.translationService.translate('cancelVisit');
  noDates = this.translationService.translate('noDates');
  username: string;
  validDates: Date[] = [];

  constructor(private httpService: HttpService, private formBuilder: FormBuilder, private snackBar: MatSnackBar,
    private translationService: TranslationService, private wowService: NgwWowService,
    private keycloakApi: KeycloakApiService) {
    this.wowService.init();
    this.username = this.keycloakApi.getUsername();
    this.filteredHeaders = JSON.parse(JSON.stringify(this.headers));
    this.filteredHeaders.pop();
    this.filteredHeaders.push(this.translationService.translate('add'));
  }

  myFilter = (d: Date): boolean => {

    return (this.validDates.find(v => v.getTime() === d.getTime())) !== undefined ? true : false;
  }

  onPaginateChange(event) {

    this.componentPending = true;
    this.page.pageNumber = event.pageIndex;
    this.page.size = event.pageSize

    this.getVisits();
  }

  getVisits() {

    const param: ServiceFilter = ({
      username: this.username,
      page: this.page,
      type: ServiceType.VISIT
    });

    this.httpService.getServiceLengthByUsername(param).pipe(concatMap(num => {

      this.yourVisitsLength = num;
      return this.httpService.getFilteredServices(param);
    })).subscribe(res => {
      this.yourVisits = res;
      this.componentPending = false;
    },
      err => {
        this.componentPending = false;
        this.snackBar.open(this.serverErr, 'OK');
        console.log(err);
      });
  }

  ngOnInit() {

    this.getVisits();

    this.visitForm = this.formBuilder.group({
      doctorName: [{ value: '', disabled: true }, Validators.required],
      license: [{ value: '', disabled: true }, Validators.required],
      date: [{ value: '', disabled: true }, Validators.required]
    });
  }

  filterVisits() {
    this.showFiltered = true;
    let nameAndSurname: string = this.visitForm.value['doctorName'];
    let splitted: string[] = nameAndSurname.split(' ');
    const param: ServiceFilter = ({
      date: this.formatDate(this.visitForm.get('date').value),
      doctorName: splitted[0],
      doctorSurname: splitted[1],
      licenseName: this.visitForm.value['license'],
      username: null,
      type: ServiceType.VISIT
    });

    this.httpService.getFilteredServices(param).subscribe(res => {
      this.filteredVisits = res;
    }, err => {
      this.snackBar.open(this.serverErr, 'OK');
      console.log(err);
    });
  }

  formatDate(date: string): string {
    const tempDate = new Date(date);
    return tempDate.getFullYear() + '.' + this.zeroNeeded(tempDate.getMonth() + 1) + '.' + this.zeroNeeded(tempDate.getDate());
  }

  zeroNeeded(num: number) {
    if (num < 10) { return '0' + num; } else { return num; }
  }

  showAddFormFn() {
    this.valid = false;
    this.licenses = [];
    this.doctors = [];
    this.visitForm.disable();
    this.visitForm.reset();
    this.showAddForm = !this.showAddForm;
    this.showFiltered = false;

    let param: LicenseFilter = {
      username: this.keycloakApi.getUsername(),
      type: LicenseType.VISIT
    }
    this.httpService.getLicenseByUsernameAndType(param).subscribe(res => this.licenses = res
      , err => {
        this.snackBar.open(this.serverErr, 'OK');
        console.log(err);
      });
    this.visitForm.controls['license'].enable();
  }

  onLicenseChoose() {

    this.showFiltered = false;
    this.valid = false;
    this.visitForm.controls['doctorName'].disable();
    this.visitForm.controls['doctorName'].setValue('');
    this.visitForm.controls['date'].disable();
    this.visitForm.controls['date'].setValue('');

    let param: DoctorLicenseFilter = {
      name: this.visitForm.value['license']
    }

    this.httpService.getDoctorsByLicense(param).subscribe(res => {
      if (res.length === 0) {
        this.snackBar.open(this.noDoctorLicense, 'OK');
      }
      this.doctors = res;
      this.visitForm.controls['doctorName'].enable();
    }, err => {
      this.snackBar.open(this.serverErr, 'OK');
      console.log(err);
    });
  }

  onDoctorChoose() {
    this.showFiltered = false;
    this.valid = false;
    this.visitForm.controls['date'].setValue('');
    this.visitForm.controls['date'].enable();

    let doctorNameSurname: string = this.visitForm.get("doctorName").value;
    let doctorNameSurnameTab: string[] = doctorNameSurname.split(" ");
    let doctorId = this.doctors.find(d => d.name === doctorNameSurnameTab[0] && d.surname === doctorNameSurnameTab[1]).id;
    let licenseId = this.licenses.find(v => v.name === this.visitForm.get("license").value).id;

    let payload: ServiceAvailability = {
      doctorId: doctorId,
      licenseId: licenseId
    }

    this.httpService.getServiceAvailability(payload).subscribe(val => {
      if (val.length === 0) {
        this.snackBar.open(this.noDates, 'OK');
      } else {
        val.forEach(v => {
          let d: Date = new Date(v);
          d.setHours(0);
          this.validDates.push(d);
        });
      }
    }, err => {
      this.snackBar.open(this.serverErr, 'OK');
      console.log(err);
    })
  }

  onDateChoose() {
    this.showFiltered = false;
    this.valid = true;
  }

  bookVisit(param: Service, wantToBook: boolean) {
    this.showAddForm = false;
    this.showFiltered = false;
    this.visitForm.reset();
    this.filteredVisits = [];

    const param1: ServiceBook = ({
      dateTime: param.time,
      username: this.username,
      doctorName: param.doctor.name,
      doctorSurname: param.doctor.surname,
      licenseName: param.license.name,
      wantToBook,
      type: ServiceType.VISIT
    });

    this.wantBook = wantToBook;

    this.httpService.bookService(param1).pipe(concatMap<boolean, any>(res1 => {

      if (res1) {
        if (wantToBook) {
          this.snackBar.open(this.bookVisitNotify, 'OK');
        } else {
          this.snackBar.open(this.cancelVisitNotify, 'OK');
        }
        if (!(this.yourVisitsLength - 1 > (this.page.pageNumber * this.page.size)) && this.page.pageNumber > 0) {
          this.page.pageNumber = this.page.pageNumber - 1;
        }

        const param2: ServiceFilter = ({
          username: this.username,
          page: this.page,
          type: ServiceType.VISIT
        });

        return this.httpService.getFilteredServices(param2);

      } else {

        return of(undefined);
      }

    }), concatMap((res: Service[] | undefined) => {

      if (typeof res !== 'undefined') {

        const param2: ServiceFilter = ({
          username: this.username,
          page: this.page,
          type: ServiceType.VISIT
        });

        this.yourVisits = res;
        return this.httpService.getServiceLengthByUsername(param2);

      } else {
        this.snackBar.open(this.serverErr, 'OK');
        console.log('error when book');
        return of(undefined);
      }

    })).subscribe(num => {

      if (typeof num !== 'undefined') {
        this.yourVisitsLength = num;
      }

    }, err => {
      this.snackBar.open(this.serverErr, 'OK');
      console.log(err);
    });
  }
}
