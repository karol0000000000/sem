import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePatientComponent } from './home-patient.component';
import { TranslatePipe } from 'src/app/service/translate.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material';
import { NgwWowModule } from 'ngx-wow';
import { CustomMaterialModule } from 'src/app/core/material.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormatDatePipe } from 'src/app/service/format-date.pipe';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { MockHttp } from 'src/app/mock/MockHttp';

describe('HomePatientComponent', () => {
  let component: HomePatientComponent;
  let fixture: ComponentFixture<HomePatientComponent>;

  const keycloakServiceSpy = jasmine.createSpyObj('KeycloakService', ['getUsername']);
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePatientComponent, TranslatePipe, FormatDatePipe ],
      imports: [ReactiveFormsModule, HttpClientModule, MatSnackBarModule, NgwWowModule, CustomMaterialModule, KeycloakAngularModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{provide: KeycloakService, useValue: keycloakServiceSpy}, { provide: HttpClient, useClass: MockHttp }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
