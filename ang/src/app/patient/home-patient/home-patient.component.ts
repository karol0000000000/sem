import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Person, Invoice, PackageItem, LicenseType } from 'src/app/models/models';
import { HttpService } from 'src/app/service/http.service';
import { MatSnackBar, MatPaginator } from '@angular/material';
import { TranslationService } from 'src/app/service/translation.service';
import { KeycloakApiService } from 'src/app/service/keycloak/keycloak-api.service';
import { concatMap } from 'rxjs/operators';

@Component({
  selector: 'app-home-patient',
  templateUrl: './home-patient.component.html',
  styleUrls: ['./home-patient.component.css']
})
export class HomePatientComponent {

  user: Person;
  packagePrice: number = 0;
  packages: PackageItem[];
  visitPackages: PackageItem[] = []
  testPackages: PackageItem[] = [];
  procedurePackages: PackageItem[] = [];
  invoices: Invoice[] = [];
  nearestInvoice: Invoice = null;
  serverErr = this.translationService.translate('serverError');
  username: string;

  constructor(private httpService: HttpService, private snackBar: MatSnackBar, private translationService: TranslationService, private keycloakApi: KeycloakApiService) {
    this.username = this.keycloakApi.getUsername();
    this.httpService.getPersonByUsername(this.username).pipe(concatMap(res => {
      this.user = res;

      return this.httpService.getInvoices(this.username);
    }), concatMap(invoices => {
      this.invoices = invoices;

      this.invoices.forEach((v: Invoice) => {

        if (this.nearestInvoice !== null) {
          this.nearestInvoice = new Date(this.nearestInvoice.deadline).getTime() > new Date(v.deadline).getTime() ? v : this.nearestInvoice;
        } else {
          this.nearestInvoice = v;
        }
      });
      return this.httpService.getPackageItemByPerson({ username: this.username });
    })).subscribe(val => {

      this.packages = val;
      this.visitPackages = this.packages.filter(p => p.license.type === LicenseType.VISIT);
      this.visitPackages = this.visitPackages.sort((a, b) => this.translationService.translate(a.name) < this.translationService.translate(b.name) ? -1 : 1);
      this.testPackages = this.packages.filter(p => p.license.type === LicenseType.TEST);
      this.testPackages = this.testPackages.sort((a, b) => this.translationService.translate(a.name) < this.translationService.translate(b.name) ? -1 : 1);
      this.procedurePackages = this.packages.filter(p => p.license.type === LicenseType.PROCEDURE);
      this.procedurePackages = this.procedurePackages.sort((a, b) => this.translationService.translate(a.name) < this.translationService.translate(b.name) ? -1 : 1);
      val.forEach(i => this.packagePrice += i.price);
    }, err => {
      this.snackBar.open(this.serverErr, 'OK');
      console.log(err);
    });
  }

}
