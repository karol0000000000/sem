import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PackageItem, PackageStatus, LicenseType } from 'src/app/models/models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgwWowService } from 'ngx-wow';
import { MatSnackBar } from '@angular/material';
import { TranslationService } from 'src/app/service/translation.service';

@Component({
  selector: 'app-register-package-choice',
  templateUrl: './register-package-choice.component.html',
  styleUrls: ['./register-package-choice.component.css']
})
export class RegisterPackageChoiceComponent implements OnInit {

  @Input()
  id;

  @Input()
  packageItems: PackageItem[];

  @Input()
  customPackageLength: number;

  @Input()
  packageStatus: PackageStatus;

  @Input()
  packageProceed: boolean;

  @Output()
  addPackageRequest: EventEmitter<PackageItem[]> = new EventEmitter();

  choicePackage: FormGroup;
  filteredPackages: PackageItem[] = [];
  licenseType: string[] = Object.keys(LicenseType);

  serverErr = this.translationService.translate('serverError');
  packageAddedSuccess = this.translationService.translate('packageGenerated');
  packageHave = this.translationService.translate('packageHave');
  packagesAddedSuccess = this.translationService.translate('packagesGenerated');
  allPossibleAdded = this.translationService.translate('allPossibleAdded');

  constructor(private wowService: NgwWowService, private formBuilder: FormBuilder,
    private snackBar: MatSnackBar, private translationService: TranslationService) {
    this.wowService.init();
    this.licenseType.push("ALL");
  }

  close() {
    this.id.hide();
    this.choicePackage.reset();
    this.filteredPackages = [];
    this.choicePackage.get('index').disable();
  }

  onPackageChoose(param: string) {

    if (param === 'ALL') {
      this.addPackageRequest.emit(this.packageItems);
    } else if (param === 'CUSTOM') {
      this.addPackageRequest.emit([this.packageItems[this.choicePackage.get("index").value]]);
    } else if (param === 'FILTER') {
      this.addPackageRequest.emit(this.filteredPackages);
    }

    let i = setInterval(() => {
      if (this.packageProceed) {
        clearInterval(i);
        this.showSumary(param);
      }
    }, 100);
  }

  showSumary(param) {

    if ((param === 'ALL' || param === 'FILTER') && this.packageStatus !== PackageStatus.FAILED) {
      this.snackBar.open(this.allPossibleAdded, 'OK');
      this.close();
    } else if (this.packageStatus === PackageStatus.ADDED) {
      this.snackBar.open(this.packageAddedSuccess, 'OK');
    } else if (this.packageStatus === PackageStatus.ALREADY_HAVE) {
      this.snackBar.open(this.packageHave, 'OK');
    } else {
      this.snackBar.open(this.serverErr, 'OK');
    }

    if (this.customPackageLength === this.packageItems.length) {
      this.close();
    }
  }

  onChooseFilterType() {

    this.choicePackage.get('index').disable();
    let type = this.choicePackage.get('type').value;

    if (type !== 'ALL') {
      this.filteredPackages = this.packageItems.filter(f => f.license.type === type);
      this.filteredPackages = this.filteredPackages.sort((a, b) =>
        this.translationService.translate(a.name) < this.translationService.translate(b.name) ? -1 : 1);
    } else {
      this.filteredPackages = this.packageItems;
      this.filteredPackages = this.filteredPackages.sort((a, b) =>
        this.translationService.translate(a.name) < this.translationService.translate(b.name) ? -1 : 1);
    }
    this.choicePackage.get('index').enable();
  }

  ngOnInit() {
    this.choicePackage = this.formBuilder.group({
      index: [{ value: '', disabled: true }, Validators.required],
      type: ['', Validators.required]
    });
  }

}
