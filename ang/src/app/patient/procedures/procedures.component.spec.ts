import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProceduresComponent } from './procedures.component';
import { TranslatePipe } from 'src/app/service/translate.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material';
import { NgwWowModule } from 'ngx-wow';
import { CustomMaterialModule } from 'src/app/core/material.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormatDatePipe } from 'src/app/service/format-date.pipe';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { MockHttp } from 'src/app/mock/MockHttp';

describe('ProceduresComponent', () => {
  let component: ProceduresComponent;
  let fixture: ComponentFixture<ProceduresComponent>;

  const keycloakServiceSpy = jasmine.createSpyObj('KeycloakService', ['getUsername']);
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProceduresComponent, TranslatePipe, FormatDatePipe ],
      imports: [ReactiveFormsModule, HttpClientModule, MatSnackBarModule, NgwWowModule, CustomMaterialModule, KeycloakAngularModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{provide: KeycloakService, useValue: keycloakServiceSpy}, { provide: HttpClient, useClass: MockHttp }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProceduresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
