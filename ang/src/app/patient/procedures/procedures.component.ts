import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSnackBar } from '@angular/material';
import { Page, License, LicenseFilter, LicenseType, DoctorLicenseFilter, Person, ServicePage, ServiceType, Service, ServiceFilter, ServiceBook, ServiceAvailability } from 'src/app/models/models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from 'src/app/service/http.service';
import { TranslationService } from 'src/app/service/translation.service';
import { NgwWowService } from 'ngx-wow';
import { KeycloakApiService } from 'src/app/service/keycloak/keycloak-api.service';
import { concatMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-procedures',
  templateUrl: './procedures.component.html',
  styleUrls: ['./procedures.component.css']
})
export class ProceduresComponent implements OnInit, AfterViewInit {

  ngAfterViewInit(): void {

    this.paginator._intl.itemsPerPageLabel = this.translationService.translate('itemsPerPageLabel');
    this.paginator._intl.nextPageLabel = this.translationService.translate('nextPageLabel');
    this.paginator._intl.previousPageLabel = this.translationService.translate('previousPageLabel');
    this.paginator._intl.firstPageLabel = this.translationService.translate('firstPageLabel');
    this.paginator._intl.lastPageLabel = this.translationService.translate('lastPageLabel');
  }

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  page: ServicePage = {
    pageNumber: 0,
    size: 10,
    type: ServiceType.PROCEDURE
  }

  yourProceduresLength: number = 0;
  yourProcedures: Service[] = [];
  filteredProcedures: Service[] = [];
  procedureForm: FormGroup;
  componentPending = true;
  validDates: Date[] = [];

  headers: string[] = [this.translationService.translate('doctor'),
  this.translationService.translate('type'),
  this.translationService.translate('time'),
  this.translationService.translate('office'),
  this.translationService.translate('cancel')];

  filteredHeaders: string[] = [];
  licenses: License[] = [];
  doctors: Person[] = [];
  showAddForm = false;
  showFiltered = false;
  wantBook = false;
  valid: boolean = false;
  serverErr = this.translationService.translate('serverError');
  noDoctorLicense = this.translationService.translate('noDoctorLicense');
  bookProcedureNotify = this.translationService.translate('bookProcedure');
  cancelProcedureNotify = this.translationService.translate('cancelProcedure');
  noDates = this.translationService.translate('noDates');
  username: string;

  constructor(private httpService: HttpService, private formBuilder: FormBuilder, private snackBar: MatSnackBar,
    private translationService: TranslationService, private wowService: NgwWowService,
    private keycloakApi: KeycloakApiService) {
    this.wowService.init();
    this.username = this.keycloakApi.getUsername();
    this.filteredHeaders = JSON.parse(JSON.stringify(this.headers));
    this.filteredHeaders.pop();
    this.filteredHeaders.push(this.translationService.translate('add'));
  }

  myFilter = (d: Date): boolean => {

    return (this.validDates.find(v => v.getTime() === d.getTime())) !== undefined ? true : false;
  }

  onPaginateChange(event) {

    this.componentPending = true;
    this.page.pageNumber = event.pageIndex;
    this.page.size = event.pageSize

    this.getProcedures();
  }

  getProcedures() {

    const param: ServiceFilter = ({
      username: this.username,
      page: this.page,
      type: ServiceType.PROCEDURE
    });

    this.httpService.getServiceLengthByUsername(param).pipe(concatMap(num => {

      this.yourProceduresLength = num;
      return this.httpService.getFilteredServices(param);
    })).subscribe(res => {
      this.yourProcedures = res;
      this.componentPending = false;
    },
      err => {
        this.componentPending = false;
        this.snackBar.open(this.serverErr, 'OK');
        console.log(err);
      });
  }

  ngOnInit() {

    this.getProcedures();

    this.procedureForm = this.formBuilder.group({
      doctorName: [{ value: '', disabled: true }, Validators.required],
      license: [{ value: '', disabled: true }, Validators.required],
      date: [{ value: '', disabled: true }, Validators.required]
    });
  }

  filterProcedures() {
    this.showFiltered = true;
    let nameAndSurname: string = this.procedureForm.value['doctorName'];
    let splitted: string[] = nameAndSurname.split(' ');
    const param: ServiceFilter = ({
      date: this.formatDate(this.procedureForm.get('date').value),
      doctorName: splitted[0],
      doctorSurname: splitted[1],
      licenseName: this.procedureForm.value['license'],
      username: null,
      type: ServiceType.PROCEDURE
    });

    this.httpService.getFilteredServices(param).subscribe(res => {
      this.filteredProcedures = res;
    }, err => {
      this.snackBar.open(this.serverErr, 'OK');
      console.log(err);
    });
  }

  formatDate(date: string): string {
    const tempDate = new Date(date);
    return tempDate.getFullYear() + '.' + this.zeroNeeded(tempDate.getMonth() + 1) + '.' + this.zeroNeeded(tempDate.getDate());
  }

  zeroNeeded(num: number) {
    if (num < 10) { return '0' + num; } else { return num; }
  }

  showAddFormFn() {
    this.valid = false;
    this.licenses = [];
    this.doctors = [];
    this.procedureForm.disable();
    this.procedureForm.reset();
    this.showAddForm = !this.showAddForm;
    this.showFiltered = false;

    let param: LicenseFilter = {
      username: this.keycloakApi.getUsername(),
      type: LicenseType.PROCEDURE
    }
    this.httpService.getLicenseByUsernameAndType(param).subscribe(res => this.licenses = res
      , err => {
        this.snackBar.open(this.serverErr, 'OK');
        console.log(err);
      });
    this.procedureForm.controls['license'].enable();
  }

  onLicenseChoose() {

    this.showFiltered = false;
    this.valid = false;
    this.procedureForm.controls['doctorName'].disable();
    this.procedureForm.controls['doctorName'].setValue('');
    this.procedureForm.controls['date'].disable();
    this.procedureForm.controls['date'].setValue('');

    let param: DoctorLicenseFilter = {
      name: this.procedureForm.value['license']
    }

    this.httpService.getDoctorsByLicense(param).subscribe(res => {
      if (res.length === 0) {
        this.snackBar.open(this.noDoctorLicense, 'OK');
      }
      this.doctors = res;
      this.procedureForm.controls['doctorName'].enable();
    }, err => {
      this.snackBar.open(this.serverErr, 'OK');
      console.log(err);
    });
  }

  onDoctorChoose() {
    this.showFiltered = false;
    this.valid = false;
    this.procedureForm.controls['date'].setValue('');
    this.procedureForm.controls['date'].enable();

    let doctorNameSurname: string = this.procedureForm.get("doctorName").value;
    let doctorNameSurnameTab: string[] = doctorNameSurname.split(" ");
    let doctorId = this.doctors.find(d => d.name === doctorNameSurnameTab[0] && d.surname === doctorNameSurnameTab[1]).id;
    let licenseId = this.licenses.find(v => v.name === this.procedureForm.get("license").value).id;

    let payload: ServiceAvailability = {
      doctorId: doctorId,
      licenseId: licenseId
    }

    this.httpService.getServiceAvailability(payload).subscribe(val => {

      if (val.length === 0) {
        this.snackBar.open(this.noDates, 'OK');
      } else {
        val.forEach(v => {
          let d: Date = new Date(v);
          d.setHours(0);
          this.validDates.push(d);
        });
      }
    }, err => {
      this.snackBar.open(this.serverErr, 'OK');
      console.log(err);
    })
  }

  onDateChoose() {
    this.showFiltered = false;
    this.valid = true;
  }

  bookProcedure(param: Service, wantToBook: boolean) {
    this.showAddForm = false;
    this.showFiltered = false;
    this.procedureForm.reset();
    this.filteredProcedures = [];

    const param1: ServiceBook = ({
      dateTime: param.time,
      username: this.username,
      doctorName: param.doctor.name,
      doctorSurname: param.doctor.surname,
      licenseName: param.license.name,
      wantToBook,
      type: ServiceType.PROCEDURE
    });

    this.wantBook = wantToBook;

    this.httpService.bookService(param1).pipe(concatMap<boolean, any>(res1 => {

      if (res1) {
        if (wantToBook) {
          this.snackBar.open(this.bookProcedureNotify, 'OK');
        } else {
          this.snackBar.open(this.cancelProcedureNotify, 'OK');
        }
        if (!(this.yourProceduresLength - 1 > (this.page.pageNumber * this.page.size)) && this.page.pageNumber > 0) {
          this.page.pageNumber = this.page.pageNumber - 1;
        }

        const param2: ServiceFilter = ({
          username: this.username,
          page: this.page,
          type: ServiceType.PROCEDURE
        });

        return this.httpService.getFilteredServices(param2);

      } else {

        return of(undefined);
      }

    }), concatMap((res: Service[] | undefined) => {

      if (typeof res !== 'undefined') {

        const param2: ServiceFilter = ({
          username: this.username,
          page: this.page,
          type: ServiceType.PROCEDURE
        });

        this.yourProcedures = res;
        return this.httpService.getServiceLengthByUsername(param2);

      } else {
        this.snackBar.open(this.serverErr, 'OK');
        console.log('error when book');
        return of(undefined);
      }

    })).subscribe(num => {

      if (typeof num !== 'undefined') {
        this.yourProceduresLength = num;
      }

    }, err => {
      this.snackBar.open(this.serverErr, 'OK');
      console.log(err);
    });
  }
}

